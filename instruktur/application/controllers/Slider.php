<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {


	public function __construct()
    {
        parent::__construct();

        // CHECK LOGIN
        if($this->session->userdata('level') != 9){
        	$this->session->set_flashdata("error","Anda harus login untuk mengakses halaman ini ");
        	redirect("../auth/login");
        }

        // MODEL
        $this->load->model("Slider_model");
    }
	

	public function index()
	{
		$data['js'] = '';
		$data['validasi'] = '';
		$data['modal'] = array($this->load->view("template/modal/tambah_slider", NULL, TRUE));

		//HASIL PROGRESS
		$data['slider'] 		= $this->Slider_model->get_slider();
		$data['folder_foto_slider']	= base_url()."../upload/foto/slider/";

		$this->load->view('template/header');
		$this->load->view('slider/index', $data);
		$this->load->view('template/footer');
	}

	public function detail($kelas = 0)
	{	
		if($kelas > 0){
			$data['js'] = '';
			$data['validasi'] = '';
			$data['modal'] = '';

			//DETAIL KELAS
			$data['slider'] 		= $this->Guru_model->get_slider_by_id($slider);
			$this->load->view('template/header');
			$this->load->view('slider/detail', $data);
			$this->load->view('template/footer');	
		}
		else
		{
			redirect("kelas");
		}
	}

	public function tambah(){
		if($_POST){
			$judul		=	$this->input->post('judul');
			$deskripsi	=	$this->input->post('deskripsi');

			$config['upload_path']          = "../upload/foto/slider/";
		    $config['allowed_types']        = 'gif|jpg|png|jpeg';
		    $config['max_size'] = '10000';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';

		    // RENAME
		    $foto 					= $judul."-".$_FILES["foto"]['name'];
		    $config['file_name']	= $foto;

			$this->load->library('upload');
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('foto')){
				echo "<script>alert('ERROR')</script>";
			} else {
			$data_slider	= array('judul' => $judul,
								'deskripsi' => $deskripsi,
								'foto' =>$foto);

			if($this->db->insert("slider", $data_slider)){
				chmod($config['upload_path'].$foto, 0777);
				$activity   =   "menambahkan slider ".$judul."".$deskripsi."".$foto;
                $this->Slider_model->write_log($activity);
				$this->session->set_flashdata("message","Berhasil menambahkan slider");
			}
			else
			{
				$this->session->set_flashdata("error","Kegagalan sistem.");
			}
		}}
		redirect("slider");
	}

	public function ubah($slider)
	{
        // JS
		$data['js']			= array('form-validator/formValidation.js', 'form-validator/bootstrap.js');
		$data['validasi']	= array($this->load->view('template/js/validasi_ubahprofil', NULL, TRUE));
		$data['modal'] = '';

		//PROFIL SISWA
		$data['slider']	= $this->Slider_model->get_slider_by_id($slider);

		$this->load->view('template/header');
		$this->load->view('slider/ubah', $data);
		$this->load->view('template/footer');
	}
	
	public function doUbah(){
		if($_POST){
			$id_slider 	= $this->input->post('id_slider');
			$judul		= $this->input->post('judul');
			$deskripsi 	= $this->input->post('deskripsi');

			$config['upload_path']          = "../upload/foto/slider/";
		    $config['allowed_types']        = 'gif|jpg|png|jpeg';
		    $config['max_size'] = '10000';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';

		    // RENAME
		    $foto 					= $judul."-".$_FILES["foto"]['name'];
		    $config['file_name']	= $foto;

			$this->load->library('upload');
			$this->upload->initialize($config);
			// UPDATE PROFIL LAIN
			if($this->upload->do_upload('foto')) {
	                // CHANGE MODE\
	                chmod($config['upload_path'].$foto, 0777); // note that it's usually changed to 0755
	                $this->Slider_model->update_slider($id_slider, $judul, $deskripsi, $foto);
					// $this->session->set_flashdata('message','<label class="label label-success clues">Ubah profil berhasil</label>');
			}else{
					$this->session->set_flashdata('message','<label class="label label-danger clues">Ubah profil gagal</label>');
			}
		}
		redirect('slider');
	}

	public function hapus($id_slider){
		$hapus = $this->Slider_model->hapus_slider($id_slider);
		if($hapus){
			$this->session->set_flashdata("error","Berhasil menghapus slider");
		}
		redirect('slider');
	}
}
