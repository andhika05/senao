<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {
	// var $folder_foto_kelas   =   base_url()."../upload/foto/kelas/";


	public function __construct()
    {
        parent::__construct();

        // CHECK LOGIN
        if(($this->session->userdata('level') != 1) && ($this->session->userdata('level') != 9)){
        	$this->session->set_flashdata("error","Anda harus login untuk mengakses halaman ini ");
        	redirect("../auth/login");
        }

        // MODEL
        $this->load->model("Guru_model");
    }
	

	public function index()
	{
		$data['js'] = '';
		$data['validasi'] = '';
		$data['modal'] = array($this->load->view("template/modal/tambah_kelas", NULL, TRUE));

		//HASIL PROGRESS
		$data['kelas'] 		= $this->Guru_model->get_kelas();
		$data['folder_foto_kelas']	= base_url()."../upload/foto/kelas/";

		$this->load->view('template/header');
		$this->load->view('kelas/index', $data);
		$this->load->view('template/footer');
	}

	public function detail($mapel = 0)
	{	
		if($mapel> 0){
			$data['js'] = '';
			$data['validasi'] = '';
			$data['modal'] = '';

			//DETAIL KELAS
			$data['mapel'] 		= $this->Guru_model->get_mapel_by_id($mapel);
			$data['siswa'] 		= $this->Guru_model->get_siswa_by_mapel($mapel);
			$data['folder_foto_siswa']	= base_url()."../upload/foto/siswa/";
			$this->load->view('template/header');
			$this->load->view('kelas/detail', $data);
			$this->load->view('template/footer');	
		}
		else
		{
			redirect("kelas");
		}
	}

	public function tambah(){
		if($_POST){
			$nama	=	$this->input->post('nama');
			$tahun	=	$this->input->post('tahun');

			$config['upload_path']          = "../upload/foto/kelas/";
		    $config['allowed_types']        = 'gif|jpg|png|jpeg';
		    $config['max_size'] = '10000';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';

		    // RENAME
		    $foto 					= $nama."-".$_FILES["foto"]['name'];
		    $config['file_name']	= $foto;

			$this->load->library('upload');
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('foto')){
				echo "<script>alert('ERROR')</script>";
			} else {
			$data_kelas	= array('nama' => $nama,
								'tahun' => $tahun,'foto'=>$foto);

			if($this->db->insert("data_kelas", $data_kelas)){
				chmod($config['upload_path'].$foto, 0777);
				$activity   =   "menambahkan kelas ".$nama." (".$tahun.")".$foto;
                $this->Guru_model->write_log($activity);
				$this->session->set_flashdata("message","Berhasil menambahkan kelas");
			}
			else
			{
				$this->session->set_flashdata("error","Kegagalan sistem.");
			}
			}
		}
		redirect("kelas");
	}

	public function ubah($kelas)
	{
        // JS
		$data['js']			= array('form-validator/formValidation.js', 'form-validator/bootstrap.js');
		$data['validasi']	= array($this->load->view('template/js/validasi_ubahprofil', NULL, TRUE));
		$data['modal'] = '';

		//PROFIL SISWA
		$data['kelas']	= $this->Guru_model->get_kelas_by_id($kelas);

		$this->load->view('template/header');
		$this->load->view('kelas/ubah', $data);
		$this->load->view('template/footer');
	}
	
	public function doUbah(){
		if($_POST){
			$id 	= $this->input->post('id');
			$nama	= $this->input->post('nama');
			$tahun 	= $this->input->post('tahun');

			$config['upload_path']          = "../upload/foto/kelas/";
		    $config['allowed_types']        = 'gif|jpg|png|jpeg';
		    $config['max_size'] = '10000';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';

		    // RENAME
		    $foto 					= $nama."-".$_FILES["foto"]['name'];
		    $config['file_name']	= $foto;

			$this->load->library('upload');
			$this->upload->initialize($config);
			// UPDATE PROFIL LAINs
			if($this->upload->do_upload('foto')) {
	                // CHANGE MODE\
	                chmod($config['upload_path'].$foto, 0777); // note that it's usually changed to 0755
	                $this->Guru_model->update_kelas($id, $nama, $tahun, $foto);
	            }
	            else{
					$this->session->set_flashdata('message','<label class="label label-danger clues">Jenis file untuk foto profil tidak didukung</label>');
	            }
			
		}
		redirect('kelas');
	}

	

	// public function mapel($kelas = 0)
	// {	
	// 	if($kelas > 0){
	// 		$data['js'] = '';
	// 		$data['validasi'] = '';
	// 		$data['modal'] = '';

	// 		//DETAIL Kelas
	// 		$data['kelas'] 				= $this->Guru_model->get_kelas_by_id($kelas);
	// 		$data['mapel'] 				= $this->Guru_model->get_mapel_by_kelas($kelas);
	// 		$data['mapel2'] 			= $this->Guru_model->get_mapel_kelas($kelas);
	// 		$data['available_mapel'] 	= $this->Guru_model->getAvailableMapelKelas($kelas);

	// 		$this->load->view('template/header');
	// 		$this->load->view('kelas/mapel', $data);
	// 		$this->load->view('template/footer');	
	// 	}
	// 	else
	// 	{
	// 		redirect("kelas");
	// 	}
	// }

	public function tambah_mapel(){
		if($_POST){
			$mapel 	=	$this->input->post('mapel');
			$kelas 	=	$this->input->post('kelas');

			// GET T_MAPEL
			$data_jadwal	=	array("t_mapel_id" => $mapel,
										"kelas_id" => $kelas,
										"tahun" => date('Y'),
										"jam" => date('H:i'));

			if($this->db->insert('t_jadwal', $data_jadwal)){
				$activity   =   "menambahkan jadwal mata kuliah ID #".$mapel." untuk kelas id #".$kelas.")";
                $this->Guru_model->write_log($activity);
				redirect("kelas/mapel/".$kelas);
			}
		}
	}

	public function hapus_kelas($idkelas){
		$hapus = $this->Guru_model->hapus_kelas($idkelas);
		if($hapus){
			$this->session->set_flashdata("error","Berhasil menghapus kelas");
		}
		redirect('kelas');
	}
}
