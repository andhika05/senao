<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksiguru extends CI_Controller {

	var $username;
	public function __construct()
    {
        parent::__construct();
        // MODEL
        $this->load->model("Guru_model");

        // CHECK LOGIN
        if(!$this->session->userdata('level')){
        	$this->session->set_flashdata("error","Anda harus login untuk mengakses halaman ini ");
        	redirect("../auth/login");
        }

        // MODEL
        $this->load->model("Transaksi_model");
    }

	public function index()
	{
		$data['js'] = '';
		$data['validasi'] = '';
		$data['modal'] = '';

		// DATA DOSEN
		$data['transaksi'] 			= $this->Transaksi_model->get_transaksiguru();
		$data['folder_foto_bukti']	= base_url()."../upload/bukti/";
		$this->load->view('template/header');
		$this->load->view('transaksiguru/index', $data);
		$this->load->view('template/footer');
	}

}
