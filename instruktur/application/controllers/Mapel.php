<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller {


	public function __construct()
    {
        parent::__construct();

        // CHECK LOGIN
        if(!$this->session->userdata('level')){
        	$this->session->set_flashdata("error","Anda harus login untuk mengakses halaman ini ");
        	redirect("../auth/login");
        }

        // MODEL
        $this->load->model("Guru_model");

        // SET USER ID
        $this->userid = $this->session->userdata('userid');
    }
	

	public function index()
	{
		$data['js'] = '';
		$data['validasi'] = '';

		$data['kelas'] 		= $this->Guru_model->get_kelas();
		$data['modal']	= array($this->load->view("template/modal/tambah_mapel", $data, TRUE));
		//$data['idmapel']	= $idmapel;

		
		$this->load->view('template/header');

		if($this->session->userdata('level') == 1){
			// DATA MAPEL UNTUK GURU
			$data['mapel'] 		= $this->Guru_model->getMapelDosen($this->userid);
			$this->load->view('mapel/index-guru', $data);	
		}
		else
		{
			// DATA MAPEL UNTUK ADMIN
			
			$data['mapel'] 				= $this->Guru_model->get_mapel_by_kelas();
			$data['folder_foto_mapel']	= base_url()."../upload/foto/mapel/";

			$this->load->view('mapel/index', $data);		
		}
		

		$this->load->view('template/footer');
	}


	public function detail($tmapel = 0)
	{	
		// TAMPILKAN MATERI DARI MAPEL DENGAN DOSEN TSB 
		if($mapel > 0){
			$data['js'] = '';
			$data['validasi'] = '';

			//DETAIL TMAPEL
			$data['mapel'] 		= $this->Guru_model->get_tmapel_materi($tmapel);
			$this->load->view('template/header');
			$this->load->view('mapel/detail', $data);
			$this->load->view('template/footer');	
		}
		else
		{
			redirect("kelas");
		}
	}

	public function tambah(){
		if($_POST){
			$nama_mapel	= $this->input->post("nama");
			$kelas = $this->input->post("kelas");
			$harga = $this->input->post("harga");

			$config['upload_path']          = "../upload/foto/mapel/";
		    $config['allowed_types']        = 'gif|jpg|png|jpeg';
		    $config['max_size'] = '10000';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';

		    // RENAME
		    $rep = str_replace(" ", "_", $nama_mapel);
		    $foto 					= $rep."-".$_FILES["foto"]['name'];
		    $config['file_name']	= $foto;

			$this->load->library('upload');
			$this->upload->initialize($config);
			if(!$this->upload->do_upload('foto')){
				echo "<script>alert('ERROR')</script>";
			} else {
			if($harga==0){
				$status = "FREE";
			} else {
				$status = "BUY";
			}
			$data_mapel	= array('nama' => $nama_mapel,
								'kelas_id' => $kelas,
								'jenis_pembayaran' => $status,
								'harga' => $harga,
								'foto'=>$foto);

			if($this->db->insert("mata_pelajaran", $data_mapel)){
				$activity   =   "menambahkan mata_pelajaran ".$nama_mapel;
                $this->Guru_model->write_log($activity);

				$this->session->set_flashdata("error","Berhasil menambahkan mata pelajaran");
			}
			else
			{
				$this->session->set_flashdata("error","Kegagalan sistem.");
			}
		}}
		else
		{
        	$this->session->set_flashdata("error","Lengkapi semua data terlebih dahulu");
		}
		redirect("mapel");
	}

	public function ubah($mapel)
	{
        // JS
		$data['js']			= array('form-validator/formValidation.js', 'form-validator/bootstrap.js');
		$data['validasi']	= array($this->load->view('template/js/validasi_ubahprofil', NULL, TRUE));
		$data['modal'] = '';

		//PROFIL SISWA
		$data['mapel']	 = 	 $this->Guru_model->get_mapel_by_id($mapel);
		$data['kelas'] 	 =	 $this->Guru_model->get_kelas();
		$harga = $this->input->post("harga");

		$this->load->view('template/header');
		$this->load->view('mapel/edit_mapel', $data);
		$this->load->view('template/footer');
	}

	public function doUbah(){
		if($_POST){
			$id 	= $this->input->post('id');
			$nama	= $this->input->post('nama');
			$harga	= $this->input->post('harga');

			$config['upload_path']          = "../upload/foto/mapel/";
		    $config['allowed_types']        = 'gif|jpg|png|jpeg';
		    $config['max_size'] = '10000';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';

		    // RENAME
		    $foto 					= $nama."-".$_FILES["foto"]['name'];
		    $config['file_name']	= $foto;

			$this->load->library('upload');
			$this->upload->initialize($config);
			// UPDATE PROFIL LAINs
			if($this->upload->do_upload('foto')) {
	                // CHANGE MODE\
	                chmod($config['upload_path'].$foto, 0777); // note that it's usually changed to 0755
	                $this->Guru_model->update_mapel($id, $nama, $foto, $harga);
	            }
	            else{
					$this->session->set_flashdata('message','<label class="label label-danger clues">Jenis file untuk foto profil tidak didukung</label>');
	            }
			
		}
		redirect('mapel');
	}

	public function tambahdosen($mapel = 0)
	{	
		
		if($mapel > 0){
			$data['js'] = '';
			$data['validasi'] = '';
			$data['modal'] = '';

			//CARI DOSEN YANG BELUM MASUK MAPEL
			$data['available'] 		= $this->Guru_model->get_dosen($mapel);
			//GET NAMA MAPEL
			$data['mapel']			= $this->Guru_model->getDetailMapel($mapel);
			$this->load->view('template/header');
			$this->load->view('mapel/tambah_dosen_mapel', $data);
			$this->load->view('template/footer');	
		}
		else
		{
			redirect("kelas");
		}
	}

	public function do_tambah_dosen(){
		if($_POST){
			$mapel	= $this->input->post("mapel");
			$dosen	= $this->input->post("dosen");

			$data_t_mapel	= array("mapel_id" => $mapel, "dosen_id" => $dosen);
			if($this->db->insert("t_mapel", $data_t_mapel)){
				$activity   =   "menambahkan dosen ID #".$dosen." ke mata pelajaran ID #".$mapel."";
                $this->Guru_model->write_log($activity);

				$this->session->set_flashdata("error","Berhasil menambahkan dosen");
			}
			else
			{
				$this->session->set_flashdata("error","Kegagalan sistem.");
			}
		}
		else
		{
        	$this->session->set_flashdata("error","Lengkapi semua data terlebih dahulu");
		}
		redirect("mapel");
	}

	public function hapusdosen($id_dosen, $id_mapel){
		$hapus 	= $this->Guru_model->hapus_dosen_mapel($id_dosen, $id_mapel);
		if($hapus){
			$this->session->set_flashdata("error","Berhasil menghapus dosen");
		}
		redirect("mapel");
	}

	public function hapus($id_mapel){
		$hapus 	= $this->Guru_model->hapus_mapel($id_mapel);
		if($hapus){
			$this->session->set_flashdata("error","Berhasil menghapus mata pelajaran");
		}
		redirect("mapel");
	}
}
