<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {


	public function __construct()
    {
        parent::__construct();

        // CHECK LOGIN
        if($this->session->userdata('level') != 9){
        	$this->session->set_flashdata("error","Anda harus login untuk mengakses halaman ini ");
        	redirect("../auth/login");
        }

        // MODEL
        $this->load->model("Transaksi_model");

    }

	public function index()
	{
		$data['js'] = '';
		$data['validasi'] = '';
		$data['modal'] = '';

		// DATA DOSEN
		$data['transaksi'] 		= $this->Transaksi_model->get_transaksi();
		$data['normal'] 		= $this->Transaksi_model->get_transaksi_normal();
		$data['folder_foto_bukti']	= base_url()."../upload/bukti/";
		$this->load->view('template/header');
		$this->load->view('transaksi/index', $data);
		$this->load->view('template/footer');
	}


	public function hapus($id = 0)
	{
		if($id > 0){
			$hapus = $this->Transaksi_model->hapus_transaksi($id);
			if($hapus){
				$this->session->set_flashdata("error", "Berhasil menghapus dosen");
			}
		}
		redirect("transaksi");
	}

	public function verif($id = 0)
	{
		if($id > 0){
			$verif = $this->Transaksi_model->verif_transaksi($id);
			if($verif){
				$this->session->set_flashdata("error", "Verifikasi Transaksi Berhasil");
			}
		}
		redirect("transaksi");
	}

}
