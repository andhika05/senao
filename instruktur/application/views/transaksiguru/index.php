<div class="container">
    <div class="row">
        <h1 class="reg-heading">Data Transaksi</h1>

    </div>
</div>

<section class="profil-guru">
    <div class="container">
        <div class="row">
            <?php
                if($this->session->flashdata("error") != ""){
                     echo "<label class='label label-danger' style='color:white;'>".$this->session->flashdata("error")."</label>";
                }
            ?>
            <?php
                if(is_array($transaksi)){
            ?>


                <table class="table table-border text-center" id="example">
                    <thead>
                        <tr>
                            <th class="text-center">Mata Pelajaran</th>
                            <th class="text-center">Nama Siswa</th>
                            <th class="text-center">Tanggal Transaksi</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Pendapatan</th>
                        </tr>    
                    </thead>
                    
            <?php
                    foreach($transaksi as $data){
            ?>
                <tfoot>
                    <tr>
                        <th><?php echo $data['nama_mapel']?></th>   
                        <th><?php echo $data['nama_siswa']?></th>    
                        <th><?php echo $data['tgl_transaksi']?></th>
                        <th><?php echo $data['status']?></th>
                        <th><?php echo "Rp ".$data['pendapatan']?></th>
                    </tr>
                </tfoot>
        <?php           } ?>
                </table>          
        </div>
        <?php
                } else {
        ?>
                    <div class="container">
                        <div class="row materi-msg">
                            <div class="item-reg text-center">
                                    <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
                            </div>
                        </div>
                    </div>
        <?php        
                }
        ?>
    </div>
</section>
    <!-- 
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>            
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
     -->
    <script>
            $(document).ready(function() {
            var t = $('#example').DataTable();
            var counter = 1;
         
            $('#addRow').on( 'click', function () {
                t.row.add( [
                    counter +'.1',
                    counter +'.2',
                    counter +'.3',
                    counter +'.4',
                    counter +'.5'
                ] ).draw( false );
         
                counter++;
            } );
         
            // Automatically add a first row of data
            $('#addRow').click();
        } );
    </script>
    