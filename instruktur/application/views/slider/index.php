<div class="container">
    <div class="row">
        <h1 class="reg-heading">Slider</h1>

    </div>
</div>

<section class="profil-guru">
    <div class="container">
        
        <div class="row">
            <span><a data-toggle="modal" href="#tambah" data-dismiss="#tambah" class="btn btn-primary">Tambah Data</a></span>
            <br/><br/>
            <?php
                if(is_array($slider)){
            ?>

                <table class="table table-no-border">
            <?php
                    foreach($slider as $data){
            ?>
                    <tr>
                        <td><?php echo $data['judul']?></td>    
                        <td><?php echo $data['deskripsi']?></td>
                        <td width="15%"><img src="<?php echo $folder_foto_slider.$data['foto']; ?>" class="img-circle img-responsive"/></td>
                        <td width="30%">
                            <a href="<?php echo base_url('slider/ubah/').$data['id_slider']?>" class="btn btn-warning">Edit Slider</a>
                            <a href="<?php echo base_url('slider/hapus/').$data['id_slider']?>" class="btn btn-danger">Hapus Slider</a>
                        </td>
                    </tr>
        <?php           } ?>
                </table>          
        </div>
        <?php
                } else {
        ?>
                    <div class="container">
                        <div class="row materi-msg">
                            <div class="item-reg text-center">
                                    <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
                            </div>
                        </div>
                    </div>
        <?php        
                }
        ?>
    </div>
</section>