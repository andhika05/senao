<div class="container">
    <div class="row">
        <h1 class="reg-heading">Form Ubah Slider</h1>
    </div>
</div>

<section>
    <div class="container">
        <div class="row reg-heading head2">
            <?php
                if($this->session->flashdata("message") != ''){
                    echo $this->session->flashdata("message");
                }
            ?> 
        </div>
    </div>
</section>

<?php
        if(is_array($slider)){
            foreach($slider as $data){
?>
<section class="form-reg">
    <div class="container">
        <form class="form-group" role="form" name="formedslider" id="formedslider" action="<?php echo base_url('slider/doUbah')?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="username" value="<?php echo $this->session->userdata('username'); ?>">
            <input type="hidden" name="id_slider" value="<?php echo $data['id_slider']; ?>">
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="judul" class="control-label">Judul</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" name="judul" class="form-control" id="judul" value="<?php echo $data['judul']; ?>">
                    <label class="clues"></label>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="deskripsi" class="control-label">Deskripsi</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" name="deskripsi" class="form-control" id="deskripsi" value="<?php echo $data['deskripsi']; ?>">
                    <label class="clues"></label>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="foto" class="control-label">Foto</label>
                </div>
            <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="file" name="foto" class="form-control" id="foto" value="<?php echo $data['foto']; ?>">
                </div>
           
            <div class="col-md-offset-3">
                <input type="submit" name="finish_reg" value="Selesai" class="btn btn-default">
            </div>
        </form>
    </div>
</section>
<?php
            }
    } else {
?>
<div class="container">
    <div class="row materi-msg">
        <div class="item-reg text-center">
                <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
        </div>
    </div>
</div>
<?php
    }
?>