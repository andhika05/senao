<div class="container">
    <div class="row">
        <h1 class="reg-heading">Data Kelas</h1>
    </div>
</div>

<section class="profil-guru">
    <div class="container">
        
        <div class="row">
            <?php
            if($kelas){
                if(is_array($kelas)){
            ?>

                <table class="table table-no-border">
            <?php
                    foreach($kelas as $data){
            ?>
                    <tr>
                        <!-- <td><?php echo $data['nama_kelas']?></td>  --> 
                        <!-- <td><?php echo getTotalSiswa($data['id_kelas'])?> Siswa </td> -->
                        <td width="30%"><a href="<?php echo base_url('kelas/detail/').$data['id_mapel']?>" class="btn btn-primary">Data Siswa</a> <a href="<?php echo base_url('rapor/kelas/').$idmapel.'/'.$data['id_mapel']?>" class="btn btn-warning">Data Nilai</a></td>
                    </tr>
        <?php           } ?>
                </table>          
        </div>
        <?php
                }
            } else {
        ?>
                    <div class="container">
                        <div class="row materi-msg">
                            <div class="item-reg text-center">
                                    <label class="label label-danger" style="color:white;">Materi/Kelas untuk Mata Pelajaran ini belum tersedia</label>
                            </div>
                        </div>
                    </div>
        <?php        
                }
        ?>
    </div>
</section>