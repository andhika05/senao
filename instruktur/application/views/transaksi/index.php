<style>
    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>
<div class="container">
    <div class="row">
        <h1 class="reg-heading">Transaksi</h1>

    </div>
</div>

<section class="profil-guru">
    <div class="container">
        <div class="row">
            <div class="tab">
              <button type="button" class="tablinks active" onclick="openCity(event, 'Transaksi')">Transaksi Sistem</button>
              <button type="button" class="tablinks" onclick="openCity(event, 'Normal')">Rekap Transaksi</button>
          </div>

          <div id="Transaksi" class="tabcontent" style="display: block;">
            <?php
            if($this->session->flashdata("error") != ""){
               echo "<label class='label label-danger' style='color:white;'>".$this->session->flashdata("error")."</label>";
           }
           ?>
           <?php
           if(is_array($transaksi)){
            ?>

            <table class="table table-border text-center">
                <tr>
                    <th class="text-center">Mata Pelajaran</th>
                    <th class="text-center">Nama Siswa</th>
                    <th class="text-center">Tanggal Transaksi</th>
                    <th class="text-center">Bukti Transfer</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Pendapatan</th>
                    <th class="text-center">Aksi</th>
                </tr>
                <?php
                foreach($transaksi as $data){
                    ?>
                    <tr>
                        <td><?php echo $data['nama_mapel']?></td>    
                        <td><?php echo $data['nama_siswa']?></td>    
                        <td><?php echo $data['tgl_transaksi']?></td>
                        <td width="13%">
                            <?php if($data['upload_bukti']==NULL){ ?>
                                <img src="<?php echo $folder_foto_bukti.'notfound.png'; ?>" class="img-circle img-responsive"/>
                            <?php } else { ?>
                                <img src="<?php echo $folder_foto_bukti.$data['upload_bukti']; ?>" class="img-circle img-responsive"/>
                            <?php } ?>
                        </td>
                        <td><?php echo $data['status']?></td>
                        <td><?php echo "Rp ".$data['pendapatan']?></td>
                        <td><a href="<?php echo base_url('transaksi/verif/').$data['id_transaksi']?>" class="btn btn-primary">Verifikasi</a> <a href="<?php echo base_url('transaksi/hapus/').$data['id_transaksi']?>" class="btn btn-danger">Hapus Transaksi</a></td>
                    </tr>
                <?php           } ?>
            </table>          
        <?php
    } else {
        ?>
        <div class="container">
            <div class="row materi-msg">
                <div class="item-reg text-center">
                    <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
                </div>
            </div>
        </div>
        <?php        
    }
    ?>
</div>

<div id="Normal" class="tabcontent" style="display: none;">
    <?php
    if($this->session->flashdata("error") != ""){
       echo "<label class='label label-danger' style='color:white;'>".$this->session->flashdata("error")."</label>";
   }
   ?>
   <?php
   if(is_array($normal)){
    ?>

    <table class="table table-border text-center">
        <tr>
            <th class="text-center">Mata Pelajaran</th>
            <th class="text-center">Nama Siswa</th>
            <th class="text-center">Tanggal Transaksi</th>
            <th class="text-center">Bukti Transfer</th>
            <th class="text-center">Status</th>
            <th class="text-center">Pendapatan</th>
        </tr>
        <?php
        $sum=0;
        foreach($normal as $data){
            ?>
            <tr>
                <td><?php echo $data['nama_mapel']?></td>    
                <td><?php echo $data['nama_siswa']?></td>    
                <td><?php echo $data['tgl_transaksi']?></td>
                <td width="13%">
                    <?php if($data['upload_bukti']==NULL){ ?>
                        <img src="<?php echo $folder_foto_bukti.'notfound.png'; ?>" class="img-circle img-responsive"/>
                    <?php } else { ?>
                        <img src="<?php echo $folder_foto_bukti.$data['upload_bukti']; ?>" class="img-circle img-responsive"/>
                    <?php } ?>
                </td>
                <td><?php echo $data['status']?></td>
                <td><b><?php echo "Rp.".number_format($data['pendapatan'],0,"",".") ?></b></td>
            </tr>
        <?php  $sum+=$data['pendapatan'];         } ?>
    </table>          
<?php
} else {
    ?>
    <div class="container">
        <div class="row materi-msg">
            <div class="item-reg text-center">
                <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
            </div>
        </div>
    </div>
    <?php        
}
?>
<h2><b>Total Pendapatan : <font style="color: green;"><?php echo "Rp.".number_format($sum,0,"",".") ?></font></b></h2>
</div>
</div>
</div>
</section>
<script>
    function openCity(evt, command) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(command).style.display = "block";
        evt.currentTarget.className += " active";
    }
    </script>