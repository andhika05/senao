<div class="container">
    <div class="row">
        <h1 class="reg-heading">Tambah Konten Materi</h1>
    </div>
</div>

<section class="form-reg">
    <div class="container">
        <form name="formtmkonten" id="formtmkonten" method="post" action="<?php echo base_url('materi/doTambahKonten') ?>" enctype="multipart/form-data" class="form-group" role="form">
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="mapel" class="control-label">Mata Pelajaran</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <select name="mapel" class="form-control" id="mapel">
                        <option value="">-- Pilih Mata Pelajaran --</option>
                        <?php foreach($mapel as $mapel){ ?>
                            <option value="<?php echo $mapel['id_mapel']?>"><?php echo $mapel['nama_mapel']?></option>
                        <?php }?>
                    </select>
                    <label class="clues">Pilih salah satu mata pelajaran</label>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="materi" class="control-label">Materi</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <select name="materi" class="form-control" id="materi">
                        <option value="">-- Pilih Materi --</option>
                        <?php foreach($materi as $materi){ ?>
                            <option value="<?php echo $materi['id']?>"><?php echo $materi['nama']?></option>
                        <?php }?>
                    </select>
                    <label class="clues">Pilih salah satu materi</label>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="submateri" class="control-label">Sub Materi</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <select name="submateri" class="form-control" id="submateri">
                        <option value="">-- Pilih Sub Materi --</option>
                        <?php foreach($submateri as $submateri){ ?>
                            <option value="<?php echo $submateri['id']?>"><?php echo $submateri['nama']?></option>
                        <?php }?>
                    </select>
                    <label class="clues">Pilih salah satu submateri</label>
                    
                </div>
            </div>
            
            <!-- <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="kategori" class="control-label">Kategori</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <select name="kategori" class="form-control" id="kategori">
                        <option value="class">Class Activity</option>
                    </select>
                    <label class="clues">Pilih kategori sesuai dengan modul yang dibuat</label>
                </div>
            </div> -->
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="keterangan" class="control-label">Keterangan</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                
                    <textarea name="keterangan" id="keterangan" rows="20"></textarea>
                    <label class="clues">Silahkan masukkan materi untuk modul anda (teks, gambar)</label>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="pdfmateri" class="control-label">Upload PDF Materi</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <input type="file" name="pdfmateri" id="pdfmateri">
                    <label class="clues">Materi dalam bentuk e-book (*.pdf)</label>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="videomateri" class="control-label">Upload Video Materi</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <input type="file" name="videomateri" id="videomateri">
                    <label class="clues">Materi dalam bentuk video (*.webm, *.ogg, *.mp4)</label>
                </div>
            </div>
            
            <div class="col-lg-offset-1 col-md-offset-2">
                <input type="submit" name="finish_reg" value="Selesai" class="btn btn-default">
            </div>
        </form>
    </div>
</section>