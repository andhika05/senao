<div class="container">
    <div class="row">
        <h1 class="reg-heading">Form Ubah Konten Materi</h1>
    </div>
</div>

<!-- <section>
    <div class="container">
        <div class="row reg-heading head2">
            <h3>Petunjuk</h3>
            <ol>
                <li>Isi form dibawah ini sesuai dengan data konten materi.</li>
                <li>Field submateri akan tampil menyesuaikan dengan pilihan pada field materi.</li>
                <li>Jika field submateri belum muncul data setelah memilih materi, harap menunggu sejenak.</li>
                <li>Untuk konten dalam bentuk teks, masukkan pada field <b>Isi Materi</b>.</li>
                <li>Untuk konten dalam bentuk e-book (*.pdf) atau video, masukkan pada field <b>Upload Materi</b>.</li>
            </ol>
        </div>
    </div>
</section> -->

<section class="form-reg">
    <div class="container">
        <?php foreach($konten as $konten) { ?>
        <form name="formtmkonten" id="formtmkonten" method="post" action="<?php echo base_url('materi/doTambahKonten') ?>" enctype="multipart/form-data" class="form-group" role="form">
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="submateri" class="control-label">Sub Materi</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <select name="submateri" class="form-control" id="submateri">
                            <option value="<?php echo $konten['submateri_id']?>" selected><?php echo getSubMateriNama($konten['submateri_id'])?>
                            </option>
                    </select>
                    <label class="clues">Pilih salah satu submateri</label>
                </div>
            </div>
            
            <!-- <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="kategori" class="control-label">Kategori</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <select name="kategori" class="form-control" id="kategori">
                        <option value="class" <?php ($konten['tipe'] == 'class') ? 'selected' : '' ?> autocomplete="off">Class Activity</option>
                        <option value="lab" <?php ($konten['tipe'] == 'lab') ? 'selected' : '' ?> autocomplete="off">Lab Activity</option>
                    </select>
                    <label class="clues">Pilih kategori sesuai dengan modul yang dibuat</label>
                </div>
            </div> -->
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="isimateri" class="control-label">Isi Materi</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <?php
                        if($konten['pdf']!=NULL){
                        $pdf = "http://localhost/e-learning/upload/materi/".$konten['pdf'];
                            ?>
                            <iframe src="http://localhost/e-learning/assets/js/pdfjs/web/viewer.html?file=<?php echo $pdf?>#zoom=page-auto" width="100%"></iframe>
                            <br/>
                        <?php
                        }
                        if($konten['video']!=NULL){
                        $video = "http://localhost/e-learning/upload/materi/".$konten['video'];
                            //$video =  realpath(dirname(__DIR__)."/../../materi".$data['konten']);
                            ?>
                            <div class="video-konten embed-responsive embed-responsive-16by9">
                                <video controls src="<?php echo $video?>" class="embed-responsive-item">
                                    Browser Anda tidak mendukung Video Player HTML5.
                                </video> 
                            </div>
                            <br/>
                        <?php 
                        }
                        ?>
                        <br>
                            <!-- FIELD INPUT -->
                            <textarea name="keterangan" id="keterangan" rows="20">
                            <?php echo $konten['keterangan'];?>
                            </textarea>
                            <label class="clues">Silahkan masukkan materi untuk modul anda (teks, gambar)</label>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="pdfmateri" class="control-label">Upload PDF Materi</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <input type="file" name="pdfmateri" id="pdfmateri">
                    <label class="clues">Materi dalam bentuk e-book (*.pdf)</label>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <label for="videomateri" class="control-label">Upload Video Materi</label>
                </div>
                <div class="col-lg-11 col-md-10 col-sm-9 col-xs-12">
                    <input type="file" name="videomateri" id="videomateri">
                    <label class="clues">Materi dalam bentuk video (*.webm, *.ogg)</label>
                </div>
            </div>
            
            <div class="col-lg-offset-1 col-md-offset-2">
                <input type="submit" name="finish_reg" value="Selesai" class="btn btn-default">
            </div>
        </form>
        <?php } ?>
    </div>
</section>