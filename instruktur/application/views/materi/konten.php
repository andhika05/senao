<section class="materi-siswa">
    <div class="container">
        <div class="row">
            <?php
                if(is_array($konten)){
                    foreach($konten as $konten){
                    ?>
            <div id="printpdf">
                <?php $id_mata=0; foreach($materi as $materi){ ?>
                <h2 class="text-center"><?php echo $materi['nama_materi']; ?></h2>
                <h3 class="text-center" id="submateri"><?php echo $materi['nama_submateri']; ?></h3>
                <div class="text-center"><?php echo $materi['keterangan_konten']; ?></div>
                <?php $id_mata=$materi['id_mata']; } ?>
                <div class="form-reg">
                    <span class="label <?php echo ($konten['tipe'] == 'class') ? 'label-success' : 'label-danger' ?>"><?php echo ucfirst($konten['tipe'])?> Activity</span>
                    <span class="label label-default">Mata Pelajaran : <?php echo $materi['nama_mapel']; ?></span>
                </div>
                <div class="text-right">
                    <a href="<?php echo base_url('rapor/kelas/').$id_mata?>" class="btn btn-danger">Nilai</a>
                    <a href="<?php echo base_url('materi/ubahkonten/').$konten['id']?>" class="btn btn-warning">Edit</a>
                    <a href="<?php echo base_url('materi/hapuskonten/').$konten['id']?>" class="btn btn-primary ">Hapus</a>
                </div>
            </div>
                <div class="form-reg">
                    <!-- konten -->
                    <?php

                        $direktori = base_url('../upload/materi/');
                        if($konten['pdf']!=NULL){
                        $pdf = $direktori.$konten['pdf'];
                            ?>
                            <div class="video-konten embed-responsive embed-responsive-16by9">
                                <iframe src="<?php echo base_url('../assets/js/pdfjs/web/')?>viewer.html?file=<?php echo $pdf?>#zoom=page-auto"></iframe>
                            </div>
                            <br>
                            <?php
                        }
                        if($konten['video']!=NULL){
                        $video = $direktori.$konten['video'];
                            //$video =  realpath(dirname(__DIR__)."/../../materi".$data['konten']);
                            ?>
                            <div class="video-konten embed-responsive embed-responsive-16by9">
                                <video controls src="<?php echo $video?>" class="embed-responsive-item">
                                    Browser Anda tidak mendukung Video Player HTML5.
                                </video> 
                            </div>
                        <?php 
                        }
                        ?>
                </div>
            </div>

            <div role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-controls="profile" class="btn btn-primary action">Download</a></li>
                    <li role="presentation"><a href="#komen" aria-controls="komen" role="tab" data-toggle="tab" class="btn btn-primary">Komentar</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <div class="siswa-selesai">
                            <span class="judul-materi">Download Materi</span>
                            <?php
                                // helper to get list siswa yang sudah upload tugas untuk mapel ini // per kelas
                                // $kelas      = backgurucode::kelas($_SESSION['user']);
                                // $finish     = backgurucode::dtraporsiswa($title['idmateri'], $kelas);
                                $tugas_siswa    =   getTugasSiswa($konten['id']);
                                $nofin      = 1;

                                // jika terdapat tugas
                                if(is_array($tugas_siswa)){
                                    foreach($tugas_siswa as $tugas){
                            ?>
                            <p>
                                <label class="label label-info"><?php echo $nofin; ?></label> 
                                <a href="<?php echo base_url('../upload/tugas/').$tugas['file_tugas']; ?>" id="<?php echo $nofin; ?>" onclick="getname(<?php echo $nofin; ?>)"><?php echo $tugas['nama_siswa']; ?></a>
                            </p>
                            <?php
                                        $nofin++;
                                    }
                                } else {
                                    ?>
                                    <p>
                                        <label class="label label-danger">Belum ada mahasiswa yang mengupload tugas</label>
                                    </p>
                                    <?php    
                                }
                            ?>
                        </div>
                    </div>
                     <div role="tabpanel" class="tab-pane" id="komen">
                        <div class="siswa-selesai">
                            <span class="judul-materi">Komentar</span>
                            <?php
                            $komentar = getKomentar($konten['id']);

                            if(is_array($komentar)){
                                foreach ($komentar as $komentar) {
                        ?>
                        <div class="comment-list">
                            <p><b><?php echo date("d F Y H:i", strtotime($komentar['tanggal'])); ?></b>, <b><?php echo getNama($komentar['user_id'], $komentar['level']) ?></b> mengatakan :</p>
                            <p><?php echo $komentar['deskripsi']?></p>
                        </div>
                        <?php 
                                }
                            }
                            else{
                                ?>
                                <label class="label label-danger">Data tidak ditemukan</label>
                                <?php
                            }
                        ?>
                        </div>
                        <div class="form-reg modul-siswa">
                            <a href="#" type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-default">Komentar Disini</a>
                        </div>
                </div>
            </div>


            <!-- Modal Dialog -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel">Komentar</h4>
                        </div>
                        <!-- ISI KOMENTAR -->
                        <form name="formkomentar" id="formkomentar" method="post" action="<?php echo base_url('materi/komentar')?>" role="form" class="form-group form-comment">
                            <p class="judul-materi">Berikan tanggapan untuk komentar mahasiswa pada materi ini!</p>
                            <!-- <input type="hidden" name="kategori" id="kategori" value="cm">
                            <input type="hidden" name="user" id="user" value="<php echo $_SESSION['user']; ?>">
                            -->
                            <label for="subyek" class="control-label">Subjek</label>
                                <input type="text" name="subyek" id="subyek" value="" class="form-control">
                            <label for="komentar" class="control-label">Komentar</label>
                                <textarea id="komentar" name="komentar" class="form-control"></textarea>
                            <!-- <label class="control-label" id="captchaOperation"></label>
                                <input type="text" class="form-control" name="captcha"/> -->
                            <input type="hidden" name="kontenmateri" id="kontenmateri" value="<?php echo $konten['id']?>"> 
                            <input type="submit" name="kirim" value="Kirim" class="btn btn-default action send">
                        </form>
                        
                        <!-- JIKA TIDAK ADA -->
                        <!--  -->
                    </div>
                </div>
            </div>
                        <?php
                            }
                            } // end if konten exist
                        ?>
            </div>
        </div>
    </div>
</section>