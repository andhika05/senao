<div class="container">
    <div class="row">
        <h1 class="reg-heading">Form Ubah Mapel</h1>
    </div>
</div>

<section>
    <div class="container">
        <div class="row reg-heading head2">
            <?php
                if($this->session->flashdata("message") != ''){
                    echo $this->session->flashdata("message");
                }
            ?> 
        </div>
    </div>
</section>

<?php
        if(is_array($mapel)){
            foreach($mapel as $data){
?>
<section class="form-reg">
    <div class="container">
        <form class="form-group" role="form" name="formedmapel" id="formedmapel" action="<?php echo base_url('mapel/doUbah')?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="username" value="<?php echo $this->session->userdata('username'); ?>">
            <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="nama" class="control-label">Nama Mapel</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" name="nama" class="form-control" id="nama" value="<?php echo $data['nama']; ?>">
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <span class="m_25"><label>Category</label></span>
                </div>
                      <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                         <select name="kelas" class="form-control">
                            <?php foreach ($kelas as $k) { ?>
                                    <option value="<?php echo $k['id']?>"><?php echo $k['nama']?></option>
                                <?php } ?>
                         </select> 
                       </div>
                </div>
                <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="harga" class="control-label">Harga</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" name="harga" class="form-control" id="harga" value="<?php echo $data['harga']; ?>">
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="file" name="foto" class="form-control" id="foto" value="<?php echo $data['foto']; ?>">
                </div>
            </div>
            
            <div class="col-md-offset-3">
                <input type="submit" name="finish_reg" value="Selesai" class="btn btn-default">
            </div>
        </form>
    </div>
</section>
<?php
            }
    } else {
?>
<div class="container">
    <div class="row materi-msg">
        <div class="item-reg text-center">
                <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
        </div>
    </div>
</div>
<?php
    }
?>