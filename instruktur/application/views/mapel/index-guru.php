<div class="col-12 px-25">
    <div class="mb-5 element-animate text-center">
        <h1>My Courses</h1>
        <p class="lead">Kursus yang saya ajar</p>
    </div>
</div>

<section class="profil-guru">
    <div class="container">
        <div class="row">
            <?php
                if(is_array($mapel)){
            ?>

                <table class="table">
            <?php
                    foreach($mapel as $mapel){
            ?>
                    <tr>
                        <td><?php echo $mapel['nama_mapel']?></td>    
                        <td width="40%">
                                <a href="<?php echo base_url('materi/bymapel/').$mapel['id_mapel']?>" class="btn btn-primary">Materi</a>
                                <a href="<?php echo base_url('kelas/detail/').$mapel['mapel_id']?>" class="btn btn-primary">Daftar Siswa</a>
                                <a href="<?php echo base_url('rapor/kelas/').$mapel['id_mapel']?>" class="btn btn-warning">Daftar Nilai</a>
                        </td>
                    </tr>
        <?php           } ?>
                </table>          
        </div>
        <?php
                } else {
        ?>
                    <div class="container">
                        <div class="row materi-msg">
                            <div class="item-reg text-center">
                                    <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
                            </div>
                        </div>
                    </div>
        <?php        
                }
        ?>
    </div>
</section>
