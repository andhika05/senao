<div class="container">
    <div class="row">
        <h1 class="reg-heading">Instruktur</h1>

    </div>
</div>

<section class="profil-guru">
    <div class="container">
        <div class="row">
            <?php
                if($this->session->flashdata("error") != ""){
                     echo "<label class='label label-danger' style='color:white;'>".$this->session->flashdata("error")."</label>";
                }
            ?>
            <?php
                if(is_array($dosen)){
            ?>

                <table class="table table-border text-center" id="example">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-center">Nama Instruktur</th>
                            <th class="text-center">Nama Pengguna</th>
                            <th class="text-center">NIP</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    
            <?php
                    foreach($dosen as $data){
            ?>
                    <tfoot>
                        <tr>
                            <td width="15%"><img src="<?php echo $folder_foto_guru.$data['foto']; ?>" class="img-circle img-responsive"/></td>
                            <td><?php echo $data['nama']?></td>    
                            <td><?php echo $data['username']?></td>    
                            <td><?php echo $data['nip']?></td>
                            <td><a href="<?php echo base_url('dosen/detail/').$data['id']?>" class="btn btn-primary">Detail Instruktur</a> <a href="<?php echo base_url('dosen/hapus/').$data['id']?>" class="btn btn-danger">Hapus Instruktur</a></td>
                        </tr>
                    </tfoot>
        <?php           } ?>
                </table>          
        </div>
        <?php
                } else {
        ?>
                    <div class="container">
                        <div class="row materi-msg">
                            <div class="item-reg text-center">
                                    <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
                            </div>
                        </div>
                    </div>
        <?php        
                }
        ?>
    </div>
</section>
    <script>
            $(document).ready(function() {
            var t = $('#example').DataTable();
            var counter = 1;
         
            $('#addRow').on( 'click', function () {
                t.row.add( [
                    counter +'.1',
                    counter +'.2',
                    counter +'.3',
                    counter +'.4',
                    counter +'.5'
                ] ).draw( false );
         
                counter++;
            } );
         
            // Automatically add a first row of data
            $('#addRow').click();
        } );
    </script>
