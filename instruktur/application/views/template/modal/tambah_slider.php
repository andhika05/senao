<!-- Modal -->
<div class="modal fade modal-white" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<form action="<?php echo base_url("slider/tambah");?>" method="post" enctype="multipart/form-data">
    <div class="modal-dialog" role="document">
        <div class="modal-content infotrophy-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah Slider</h4>
            </div>
            <div class="modal-body">
                <div id="container_update">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12 col-lg-12 controls">
                                <span class="m_25"><label>Judul</label></span>
                                <input type="text" name="judul" id="judul" class="form-control" value="" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12 col-lg-12 controls">
                                <span class="m_25"><label>Deskripsi</label></span>
                                <input type="text" name="deskripsi" id="deskripsi" class="form-control" value="" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12 col-lg-12 controls">
                                <span class="m_25"><label>Foto</label></span>
                                <input type="file" name="foto" id="foto" class="form-control" value="" placeholder="Foto Slider">
                            </div>
                        </div>
                    </div>
                </div>                            
                               
            </div>
          <!-- end modal-body -->
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">CANCEL</button>
                <input type="submit" class="btn btn-info " value="SIMPAN" name="submit" >
            </div>
        </div>
        <!-- end modal-content -->
    </div>
</form>    
</div>
<!-- END Modal-->


