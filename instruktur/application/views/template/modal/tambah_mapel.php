<!-- Modal -->
<div class="modal fade modal-white" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<form action="<?php echo base_url("mapel/tambah");?>" method="post" enctype="multipart/form-data">
    <div class="modal-dialog" role="document">
        <div class="modal-content infotrophy-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah Mata Pelajaran</h4>
            </div>
            <div class="modal-body">
                <div id="container_update">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-12 col-lg-12 controls">
                                <span class="m_25"><label>Nama Mata Pelajaran</label></span>
                                <input type="text" name="nama" id="nama" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 col-lg-12 controls">
                                <span class="m_25"><label>Category</label></span>
                                    <select name="kelas" class="form-control">
                                        <?php foreach ($kelas as $k) { ?>
                                    <option value="<?php echo $k['id']?>"><?php echo $k['nama']?></option>
                                <?php } ?>
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 col-lg-12 controls">
                                <span class="m_25"><label>Harga</label></span>
                                <input type="text" name="harga" id="harga" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 col-lg-12 controls">
                                <span class="m_25"><label>Foto</label></span>
                                <input type="file" name="foto" id="foto" class="form-control" value="">
                            </div>
                        </div>
                    </div>
                </div>                            
                               
            </div>
          <!-- end modal-body -->
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">CANCEL</button>
                <input type="submit" class="btn btn-info " value="SIMPAN" name="submit" >
            </div>
        </div>
        <!-- end modal-content -->
    </div>
</form>    
</div>
<!-- END Modal-->


