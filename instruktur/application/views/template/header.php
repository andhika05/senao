<html>
    <head>
        <?php $halaman = ucfirst($this->router->fetch_class()); ?>
        <title> Area <?php echo $this->session->userdata('level') == 9 ? 'Admin | Manajemen ' : 'Dosen | '; ?><?php echo $halaman ?></title>
        
        <!-- Meta Tags -->
        <meta charset="UTF-8">
        <meta name="description" content="Gloed">
        <meta name="keywords" content="Courses, Class, About,">
        <meta name="author" content="Tim Gloed">
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        
        <!-- CSS -->

        <base href="http://<?php echo $_SERVER['HTTP_HOST'];?>/e-learning/instruktur/dosen">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>bootstrap.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>style.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>guru.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>guru.css"/>        

        <!-- Icons -->
<!--         <link rel="icon" href="<?php echo base_url('assets/images/')?>logo1.png">
 -->        
        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        
    </head>
    <body>
        <header>
            <div class="navigation">
                <div class="container">
                    <div class="row">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand visible-xs" href="#">E-Learning</a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                              
                                    <?php
                                        $halaman        =   strtolower($halaman);
                                        if($halaman == '')  $halaman    =   'profil';

                                        $classbn        = "";
                                        $spanclassbn    = "";
                                        $classprof      = "";
                                        $spanclasspr    = "";
                                        $classrapor     = "";
                                        $spanclassrp    = "";
                                        $classmateri    = "";
                                        $spanclassmt    = "";
                                        $classnote      = "";
                                        $spannote       = "";

        
                                    ?>
                                    <ul class="nav navbar-nav">
                                    <?php
                                        if($this->session->level == 1){ ?>
                                        <li class="<?php echo $halaman == 'profil' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('profil');?>">Profile</a>
                                            <span class="<?php echo $halaman == 'profil' ? 'profil' : '' ?>"></span>
                                        </li>
                                        <li class="<?php echo $halaman == 'mapel' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('mapel');?>">My Courses</a>
                                            <span class="<?php echo $halaman == 'mapel' ? 'mapel' : '' ?>"></span>
                                        </li>
                                        <li class="<?php echo $halaman == 'materi' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('materi');?>">Materi</a>
                                            <span class="<?php echo $halaman == 'materi' ? 'materi' : '' ?>"></span>
                                        </li>
                                        <!-- <li class="<?php echo $halaman == 'rapor' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('rapor'); ?>">Daftar Nilai</a>
                                            <span class="<?php echo $halaman == 'rapor' ? 'clue' : '' ?>"></span>
                                        </li> -->
                                        <li class="<?php echo $halaman == 'transaksiguru' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('transaksiguru'); ?>">Transactions</a>
                                            <span class="<?php echo $halaman == 'transaksiguru' ? 'clue' : '' ?>"></span>
                                        </li>
                                    <?php } ?>
                                    <?php if($this->session->level == 9){ ?>
                                        
                                        </li>
                                        <li><a href="<?php echo base_url('dosen')?>">
                                            <label class="label label-success">
                                                <?php echo $this->session->userdata("username"); ?></label></a></li>
                                        <li class="<?php echo $halaman == 'dosen' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('dosen');?>">Instructors</a>
                                            <span class="<?php echo $halaman == 'dosen' ? 'note' : '' ?>"></span>
                                        </li>
                                        <li class="<?php echo $halaman == 'kelas' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('kelas');?>">Categories</a>
                                            <span class="<?php echo $halaman == 'kelas' ? 'note' : '' ?>"></span>
                                        </li>
                                        <li class="<?php echo $halaman == 'mapel' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('mapel');?>">Mapel</a>
                                            <span class="<?php echo $halaman == 'mapel' ? 'note' : '' ?>"></span>
                                        </li>
                                        <li class="<?php echo $halaman == 'transaksi' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('transaksi');?>">Transactions</a>
                                            <span class="<?php echo $halaman == 'transaksi' ? 'note' : '' ?>"></span>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Tools<span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="<?php echo base_url('slider') ?>">Slider</a>
                                                </li>
                                                <!-- <li>
                                                    <a href="<?php echo base_url('dosen') ?>">Dosen</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo base_url('kelas') ?>">Kelas</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<?php echo base_url('mapel') ?>">Mata Kuliah</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="<php echo base_url('jadwal') ?>">Jadwal</a>
                                                </li> -->
                                            </ul>
                                    <?php } ?>
                                    </ul>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li>
                                            <a href="<?php echo base_url('../auth/login')?>">Keluar</a>
                                        </li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
        </header>