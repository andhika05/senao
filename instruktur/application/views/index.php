<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Document Reminder</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <!--base css styles-->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/assets/font-awesome/css/font-awesome.min.css">

        <!--page specific css styles-->

        <!--flaty css styles-->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flaty.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/flaty-responsive.css">

        <link rel="shortcut icon" href="<?php echo base_url()?>assets/img/favicon.png">
    </head>
    <body>


        <!--basic scripts-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url()?>assets/jquery/jquery-2.1.4.min.js"><\/script>')</script>
        <script src="<?php echo base_url()?>assets/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>assets/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="<?php echo base_url()?>assets/assets/jquery-cookie/jquery.cookie.js"></script>

        <!--page specific plugin scripts-->


        <!--flaty scripts-->
        <script src="<?php echo base_url()?>assets/js/flaty.js"></script>
        <script src="<?php echo base_url()?>assets/js/flaty-demo-codes.js"></script>

    </body>
</html>
