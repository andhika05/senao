<!-- <div class="container">
    <div class="row">
        <?php if($mapel) { foreach($mapel as $mapel) {?>
        <h1 class="reg-heading">Mahasiswa Kelas <?php echo $mapel['nama']." (".$mapel['tahun']."/".($mapel['tahun']+1).")" ?></h1>
        <?php }} ?>
    </div>
</div> -->

<div class="col-12 px-25">

                    <div class="mb-5 element-animate text-center">
                      <h1>Data Siswa</h1>
                       <p class="lead">Siswa yang mengikuti kursus</p>
                    </div>
                </div>

<section class="profil-guru">
    <div class="container">
        <div class="row">
            <?php
                if(is_array($siswa)){
            ?>
                <table class="table table-border text-center">
                    <tr>
                        <th colspan="2" class="text-center">Nama</th>
                        <th class="text-center">email</th>
                        <th class="text-center">ktp</th>
                        
                    </tr>
            <?php
                    foreach($siswa as $data){
            ?>
                    <tr>
                        <td width="10%"><img src="<?php echo $folder_foto_siswa.$data['foto']; ?>" class="img-circle" width="25px"/></td> 
                        <td><?php echo $data['nama_siswa']?></td>
<!--                         <td><?php echo $data['username']?></td>
 -->                        <td><?php echo $data['email']?></td>
                        <td><?php echo $data['nim']?></td>
                        
                    </tr>
        <?php           } ?>
                </table>          
        </div>
        <?php
                } else {
        ?>
                    <div class="container">
                        <div class="row materi-msg">
                            <div class="item-reg text-center">
                                    <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
                            </div>
                        </div>
                    </div>
        <?php        
                }
        ?>
    </div>
</section>