<div class="container">
    <div class="row">
        <h1 class="reg-heading">Kategori</h1>

    </div>
</div>

<section class="profil-guru">
    <div class="container">
        
        <div class="row">
            <span><a data-toggle="modal" href="#tambah" data-dismiss="#tambah" class="btn btn-primary">Tambah Kategori</a></span>
            <br/><br/>
            <?php
                if(is_array($kelas)){
            ?>

                <table class="table table-no-border">
            <?php
                    foreach($kelas as $data){
            ?>
                    <tr>
                        <td><?php echo $data['nama']?></td>    
                        <td width="25%"><img src="<?php echo $folder_foto_kelas.$data['foto']; ?>" class="img-circle img-responsive"/></td>
                        <td width="30%">
                            <!-- <a href="<?php echo base_url('kelas/mapel/').$data['id']?>" class="btn btn-primary">Data Mata Pelajaran</a> -->
                            <a href="<?php echo base_url('kelas/ubah/').$data['id']?>" class="btn btn-warning">Edit</a>
                            <a href="<?php echo base_url('kelas/hapus/').$data['id']?>" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
        <?php           } ?>
                </table>          
        </div>
        <?php
                } else {
        ?>
                    <div class="container">
                        <div class="row materi-msg">
                            <div class="item-reg text-center">
                                    <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
                            </div>
                        </div>
                    </div>
        <?php        
                }
        ?>
    </div>
</section>