<?php
    class Slider_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
                $CI =& get_instance();
        }

        public static function randompassword($password){
            $random     = "x0e7q5t1k3g8s2n4lr9f";
            $randompass = sha1(md5($random.md5($password).$random));
            return $randompass;
        }

        public static function get_slider(){
            $CI     =& get_instance();
            $where  =   array('status' => 1 );
            return $CI->db->get_where("slider", $where)->result_array();;
        }

        public function tampil_slider($limit){
        return $this->db->get('slider');
        }
        public static function get_slider_by_id($slider){
            $CI     =& get_instance();
            $where  = array("id_slider" => $slider);
            return $CI->db->get_where("slider", $where)->result_array();;
        }
        
        public static function update_slider($id_slider, $judul, $deskripsi, $foto){
            $CI     =& get_instance();
            $foto_fix = str_replace(" ", "_", $foto);
            $slider  =   array("judul" => $judul, "deskripsi" => $deskripsi, "foto" => $foto_fix);
            $where  = array("id_slider" => $id_slider);
            
            $CI->db->where($where);
            if($CI->db->update("slider", $slider)){
                $activity   =   "mengupdate slider untuk slider ID #".$id_slider;
                $CI->Slider_model->write_log($activity);
                return true;
            }
            else{
                return false;
            }
        }

        public static function write_log($activity){
            $CI =& get_instance();

            $username   =   $CI->session->userdata('username');

            date_default_timezone_set("Asia/Jakarta");
            $time           = date("Y-m-d H:i:s");
            
            $log_data       = array("time" => $time, "username" => $username, "description" => $activity);
            if($CI->db->insert("activity_log", $log_data)){
                return true;
            }
            else{
                return false;
            }
        }

        public static function hapus_slider($slider){
            $CI     =& get_instance();

            $hapus  =   array('status' => 0);
            $where  =   array("id_slider" => $slider);

            $CI->db->where($where);
            if($CI->db->update("slider", $hapus)){
                //write log
                $activity   =   "menghapus slider ID #".$slider;
                $CI->Slider_model->write_log($activity);
                return true;
            }
            return false;
        }


           public static function get_logs(){
            $CI     =& get_instance();
            $log    =   $CI->db->order_by('time', 'DESC')->get('activity_log')->result_array();
            return $log;
        }
    }
?>