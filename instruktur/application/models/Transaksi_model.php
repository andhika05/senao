<?php
    class Transaksi_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
                $CI =& get_instance();
        }

        public static function get_transaksi(){
            $CI     =& get_instance();
            $dosen  = $CI->db->select("transaksi.*, mata_pelajaran.nama as nama_mapel, mata_pelajaran.harga as harga, data_siswa.nama as nama_siswa, ROUND(mata_pelajaran.harga * 30/100) as pendapatan")
                        ->from("transaksi")
                        ->join("mata_pelajaran", "mata_pelajaran.id = transaksi.id_mapel")
                        ->join("data_siswa", "data_siswa.id = transaksi.id_siswa")
                        ->where("status_data = 1")->get()->result_array();
            return $dosen;
        }

        public static function get_transaksi_normal(){
            $CI     =& get_instance();
            $dosen  = $CI->db->select("transaksi.*, mata_pelajaran.nama as nama_mapel, mata_pelajaran.harga as harga, data_siswa.nama as nama_siswa, mata_pelajaran.harga as pendapatan")
                        ->from("transaksi")
                        ->join("mata_pelajaran", "mata_pelajaran.id = transaksi.id_mapel")
                        ->join("data_siswa", "data_siswa.id = transaksi.id_siswa")
                        ->where("status_data = 1")->get()->result_array();
            return $dosen;
        }

        public static function get_transaksiguru(){
            $CI     =& get_instance();
            $dosen  = $CI->db->select("transaksi.*, mata_pelajaran.nama as nama_mapel,      mata_pelajaran.harga as harga, data_siswa.nama as nama_siswa, ROUND(mata_pelajaran.harga * 70/100) as pendapatan")
                        ->from("transaksi")
                        ->join("mata_pelajaran", "mata_pelajaran.id = transaksi.id_mapel")
                        ->join("data_siswa", "data_siswa.id = transaksi.id_siswa")
                        ->join("t_mapel","t_mapel.mapel_id = transaksi.id_mapel")
                        ->where("t_mapel.dosen_id",$CI->session->userdata('userid'))
                        ->get()->result_array();
            return $dosen;
        }

        public static function hapus_transaksi($transaksi){
            $CI     =& get_instance();

            $hapus = array("status_data" => 0);
            $where = array("id_transaksi" => $transaksi);
            $CI->db->where($where);
            if($CI->db->update("transaksi", $hapus)){
                //write log
                // $activity   =   "menghapus dosen ID #".$dosen;
                // $CI->Guru_model->write_log($activity);
                return true;
            }
            return false;
        }

        public static function verif_transaksi($transaksi){
            $CI     =& get_instance();

            $verif = array("status" => "Sudah Verifikasi");
            $where = array("id_transaksi" => $transaksi);
            $CI->db->where($where);
            if($CI->db->update("transaksi", $verif)){
                //write log
                // $activity   =   "menghapus dosen ID #".$dosen;
                // $CI->Guru_model->write_log($activity);
                return true;
            }
            return false;
        }
    }
?>