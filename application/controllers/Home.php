<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	
	public function __construct()
    {
        parent::__construct();

        // MODEL
    	$this->load->model('Front_model');
        $this->load->model("Guru_model");
    }

	public function index()
	{
		$data['js'] 	 		= '';
		$data['validasi'] 		= '';
		$data['modal']	  		= '';
		$data['kelas']	  		= $this->Guru_model->get_kelas();
		$data['mapel_top'] 		= $this->Guru_model->mapel_top();
		$data['slider']   		= $this->Front_model->tampil_slider(6)->result_array();
		$data['testimoni'] 		= $this->Front_model->get_profil();
		$this->load->view('template/headertemp');
		$this->load->view('home/index', $data);
		$this->load->view('template/footer');
	}

	public function courses()
	{
		$data['js'] 	  = '';
		$data['validasi'] = '';
		$data['modal'] 	  = '';	
		if($this->session->userdata('userid') != NULL) {
			$data['mapel_limit'] 		= $this->Guru_model->ceklikelimit($this->session->userdata('userid'));
		} else {
			$data['mapel_limit'] 		= $this->Guru_model->mapel_limit();
		}	
		$this->load->view('template/headertemp');
		$this->load->view('home/courses', $data);
		$this->load->view('template/footer');
	}

	public function Like($id)
	{
		if($this->session->userdata('level')==2){
			$this->Front_model->like($id, $this->session->userdata('userid'));
		} else {
			redirect(base_url().'auth/login');
		}
		redirect(base_url().'home/courses');
	}

	public function Dislike($id)
	{
		if($this->session->userdata('level')==2){
			$this->Front_model->dislike($id, $this->session->userdata('userid'));
		} else {
			redirect(base_url().'auth/login');
		}
		redirect(base_url().'home/courses');
	}
	
	public function allcourses()
	{
		$data['js'] 	  = '';
		$data['validasi'] = '';
		$data['modal'] 	  = '';		
		if($this->session->userdata('userid') != NULL) {
			$data['mapel'] 		= $this->Guru_model->ceklike($this->session->userdata('userid'));
		} else {
			$data['mapel'] 		= $this->Guru_model->get_mapelallcourses();
		}			
		$this->load->view('template/headertemp');
		$this->load->view('home/allcourses', $data);
		$this->load->view('template/footer');
	}

	public function categories()
	{
		$data['js'] 	  = '';
		$data['validasi'] = '';
		$data['modal'] 	  = '';
		$data['kelas']    = $this->Guru_model->kelas_limit();

		$this->load->view('template/headertemp');
		$this->load->view('home/categories', $data);
		$this->load->view('template/footer');
	}


	public function allcategories()
	{
		$data['js'] 	  = '';
		$data['validasi'] = '';
		$data['modal'] 	  = '';
		$data['kelas']    = $this->Guru_model->get_kelas();

		$this->load->view('template/headertemp');
		$this->load->view('home/allcategories', $data);
		$this->load->view('template/footer');
	}

		public function showcategories($kelas)
	{
		$data['js'] 	  = '';
		$data['validasi'] = '';
		$data['modal'] 	  = '';
		$data['mapel']    = $this->Guru_model->get_mapel_by_kelas($kelas);

		$this->load->view('template/headertemp');
		$this->load->view('home/showcategories', $data);
		$this->load->view('template/footer');
	}

	public function singlecourses($mapel)
	{
		$data['js'] 	  = '';
		$data['validasi'] = '';
		$data['modal'] 	  = array($this->load->view("template/modal/modal_sukses", NULL, TRUE));
		if($this->session->userdata('userid') != NULL) {
			$data['mapel'] 	  = $this->Guru_model->get_mapel_by_id($mapel);
			$data['trans'] 	  = $this->Guru_model->get_transaksi_by_id($mapel, $this->session->userdata('userid'));
			$data['materi']   = $this->Guru_model->get_materi_mapel($mapel);
		} else {
			$data['mapel'] 	  = $this->Guru_model->get_mapel_by_id($mapel);
			$data['materi']   = $this->Guru_model->get_materi_mapel($mapel);
		}

		$this->load->view('template/headertemp');
		$this->load->view('home/singlecourses', $data);
		$this->load->view('template/footer');
	}

	public function about()
	{
		$data['js'] 		= '';
		$data['validasi'] 	= '';
		$data['modal'] 		= '';
		$data['kelas'] 		= $this->Guru_model->get_kelas();
		$this->load->view('template/headertemp');
		$this->load->view('home/about', $data);
		$this->load->view('template/footer');
	}

	public function contact()
	{	$data['js'] 		= '';
		$data['validasi'] 	= '';
		$data['modal'] 		= '';
		$this->load->view('template/headertemp');
		$this->load->view('home/contact', $data);
		$this->load->view('template/footer');
	}

	public function register()
	{	$data['js'] 		= '';
		$data['validasi'] 	= '';
		$data['modal'] 		= '';
		$this->load->view('template/headertemp');
		$this->load->view('home/register', $data);
		$this->load->view('template/footer');
	}


	public function send()
	{
		$subject	="email dari -  ".$this->input->post("name");
		$file_data	=$this->upload_file();

		if(is_array($file_data))
		{
			$message='
			<h3 align="center">Email</h3>
			<table border="1" width="100%" cellpadding="2">
				<tr>
					<td>Name</td>
					<td>'.$this->input->post("name").'</td>
				</tr>
				<tr>
					<td>Address</td>
					<td>'.$this->input->post("address").'</td>
				</tr>
				<tr>
					<td>Email Address</td>
					<td>'.$this->input->post("email").'</td>
				</tr>
				<tr>
					<td>Additional Information</td>
					<td>'.$this->input->post("additional_information").'</td>
				</tr>
			</table>
			';
			$config = array(
				'protocol'	=> 'smtp',
				'smtp_host'	=>	'ssl://smtp.gmail.com',
				'smtp_port'	=> 465,
				'smtp_user' =>'maulanaendarta@gmail.com',
				'smtp_pass' =>'7abarBasith96',
				'mailtype'	=>'html',
				'charset'	=>'iso-8859-1',
				'wordwrap'	=>TRUE
				);

			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from($this->input->post("email"));
			$this->email->to('maulanaendarta@gmail.com');
			$this->email->subject($subject);
			$this->email->message($message);
			$this->email->attach($file_data['full_path']);
			if ($this->email->send())
			{
			 	 if(delete_files($file_data['file_path']))
				{
					$this->session->set_flashdata('message','Email Sended');
			 			redirect('home');
			 	 }
			} 

		}
		else
		{
			$this->session->set_flashdata('message', 'There is an error in attach file');
		}

	}

	function upload_file()
	{
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'doc|docx|pdf';
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('resume'))
		{
			return $this->upload->data();
		}
		else
		{
			return $this->upload->display_errors();
		}
	}

	public function Booking($id)
	{
		if($this->session->userdata('level')==2){
			$this->Front_model->insert_transaksi($id, $this->session->userdata('userid'));
		} else {
			redirect(base_url().'auth/login');
		}
		redirect(base_url().'siswa/beranda/log_mapel');
	}

	public function latihan(){
		$data['js'] 	  = '';
		$data['validasi'] = '';
		$data['modal']	  = '';
		$this->load->view('home/coba', $data);
	}


}
