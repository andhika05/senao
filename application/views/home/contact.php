
    
<body>   
  <section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo base_url()?>assets/images/big_image_1.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-8 text-center">

            <div class="mb-5 element-animate">
              <h1>Contact Us </h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->


  <!-- <form action="<?php echo base_url();?>home/send" method="post" enctype="multipart/form-data" accept-charset="utf-8"> -->
  <?php
  if($this->session->flashdata("message"))
  {
    echo "
    <div class='alert alert-success'>
    ".$this->session->flashdata("message")."
    </div>";
  }
  ?>  


  <section class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
             <form method="post" action="<?php echo base_url();?>home/send" enctype="multipart/form-data">
                <div class="form-group">
                  <label>Enter Name</label>
                  <input type="text" name="name" placeholder="Enter Name" class="form-control" required  />
                </div>
                <div class="form-group">
                  <label>Enter Address</label>
                  <textarea name="address" placeholder="Enter Address" class="form-control" required></textarea> 
                </div>
                <div class="form-group">
                  <label>Enter Email</label>
                  <input type="email" name="email" class="form-control" placeholder="Enter Email Address" required/>
                </div>
              <div class="form-group">
                <label>File input</label>
                <input type="file" name="resume" accept=".doc,.docx,.pdf" required/>
              </div>
              <div class="form-group">
                <label>Body Content</label>
                <textarea name="additional information" placeholder="Enter Additional Information" class="form-control" required rows="5"></textarea>
              </div>
              <div class="form-group" align="center">
                <input type="submit" name="submit" value="submit" class="btn btn-info"/>
  </form> 
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

      <footer class="site-footer" style="background-image: url(<?php echo base_url()?>assets/images/big_image_3.jpg);">

  
    
    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="<?php echo base_url('assets/')?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery-migrate-3.0.0.js"></script>
    <script src="<?php echo base_url('assets/')?>js/popper.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.stellar.min.js"></script>

    
    <script src="<?php echo base_url('assets/')?>js/main.js"></script>
  </body>

<!-- Mirrored from technext.github.io/skwela/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 20:58:38 GMT -->
</html>