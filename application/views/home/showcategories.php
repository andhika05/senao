    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/bootstrap.min.css">

    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/font-awesome.min.css">

    <!-- ElegantFonts CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/elegant-fonts.css">

    <!-- themify-icons CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/themify-icons.css">

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/swiper.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/stylecourses.css">

  <body>
  
    <section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo base_url()?>assets/images/big_image_1.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-8 text-center">

            <div class="mb-5 element-animate">
              <h1>Find A Courses And Keep Learn </h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->
       
<?php
    if(is_array($mapel)){ 
 ?>
<section class="featured-courses vertical-column courses-wrap">
        <div class="container">
            <div class="row mx-m-25">
                <div class="col-12 px-25">

            <div class="mb-5 element-animate text-center">
              <h1>Show Categories Courses</h1>
                 <!-- <p class="lead">Kursus yang dapat anda ikuti</p> -->

            </div>
                </div>

            <?php foreach($mapel as $data){?>
                <div class="col-12 col-md-6 col-lg-4 px-25">
                    <div class="course-content">
                        <figure class="course-thumbnail">
                          <br>
                            <a href="<?php echo base_url('home/singlecourses/'.$data['id'])?>" class="course"> 
                                <img src="<?php echo base_url('upload/foto/mapel/'.$data['foto']) ?>" class="img-responsive " alt="" style="height: 180px;"></a>
                        </figure><!-- .course-thumbnail -->

                        <div class="course-content-wrap">
                            <div class="entry-title">
                                <a href="<?php echo base_url('home/singlecourses/'.$data['id'])?>" class="course"> 
                                <h3 style="text-align: center;"><?php echo $data['nama']?></h3></a></div>
                            <footer class="entry-footer flex justify-content-between align-items-center">
                                
                                <div class="course-cost">
                                    <?php if($data['jenis_pembayaran'] == "BUY") {
                                        echo "Rp. ".number_format($data['harga'],0,",",".");
                                        } else {
                                            echo $data['jenis_pembayaran'];
                                        } ?> <!-- <span class="price-drop">$68</span> -->
                                </div><!-- .course-cost -->

                                <div class="course-ratings flex justify-content-end align-items-center">
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star-o"></span>

                                    <span class="course-ratings-count">(4 votes)</span>
                                </div><!-- .course-ratings -->
                            </footer><!-- .entry-footer -->
                        </div><!-- .course-content-wrap -->
                    </div><!-- .course-content -->
                </div><!-- .col --><?php           } ?>
                <?php           } ?>
                
            </div><!-- .row -->
        </div><!-- .container -->                            
    </section><!-- .courses-wrap -->

  <footer class="site-footer" style="background-image: url(<?php echo base_url()?>assets/images/big_image_3.jpg);">
    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <!-- Anyar -->
    <script src='<?php echo base_url('assets/')?>js/jquery.js'></script>
    <script src='<?php echo base_url('assets/')?>js/swiper.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/masonry.pkgd.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/jquery.collapsible.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/custom.js'></script>
    
    <!-- lawas -->
    <script src="<?php echo base_url('assets/')?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery-migrate-3.0.0.js"></script>
    <script src="<?php echo base_url('assets/')?>js/popper.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.stellar.min.js"></script>

    <script src="<?php echo base_url('assets/')?>js/main.js"></script>
  </body>

</html>