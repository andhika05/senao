<br><div class="bg-reg">
    <div class="container">
        <div class="row">
            <h1 class="reg-heading"> Form Pendaftaran Instruktor</h1> 
        </div>
    </div>
</div>

<section class="form-reg">
    <div class="container">
        <form name="registrasi" id="registrasi" method="post" action="<?php echo base_url('auth/doregistrasiguru')?>" enctype="multipart/form-data" class="form-group" role="form">
            <div class="row item-reg">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php
                        if($this->session->flashdata("error") != ""){
                             echo "<label class='label label-danger' style='color:white;'>".$this->session->flashdata("error")."</label>";
                        }
                    ?>
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="pengguna" class="control-label">Username</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" id="pengguna" name="pengguna" class="form-control" value="">
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="nama" class="control-label">Nama Lengkap</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" id="nama" name="nama" class="form-control" value="">
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="nip" class="control-label">KTP</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" name="nip" id="nip" class="form-control" value="">
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="mail" class="control-label">Email</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" name="mail" class="form-control" id="mail" value="">
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="mail" class="control-label">Foto Profil</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 foto-profil">
                    <input name="profil" id="profil" type="file">
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="kunci" class="control-label">Kata Kunci</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="password" name="kunci" class="form-control" id="kunci" value="">
                </div>
            </div>
            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="ulangikunci" class="control-label">Ulangi Kata Kunci</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="password" name="ulangikunci" class="form-control" id="ulangi_kunci" value="">
                </div>
            </div>
            <div class="col-md-offset-3"><br>
                <input type="submit" name="finish_reg" value="Selesai" class="btn btn-default action" style="margin-left: 25.6%;">
            </div>
        </form>
    </div>

</section>
          <footer class="site-footer" style="background-image: url(../images/big_image_3.jpg);">
