<style>

	@media screen and (max-width: 5000px){
		#ukuran{
			margin:0 32%;
		}
		#ukuran1{
			margin-left: -32%;
		}
		a{
			text-align: center;
		}
	}

	@media screen and (max-width: 700px){
		#ukuran{
			margin:0 32%;
			width: 50%
		}
		#ukuran1{
			margin-left: 32%;
			width: 36%
		}
	}





</style> 
    <section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(../images/big_image_1.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-8 text-center">

            <div class="mb-5 element-animate">
              <h1>Why We Started E-Learning Business</h1>
            </div>
          </div>
        </div>
      </div>
    </section>
<!-- END section -->

<section class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2>Registrasi</h2>
            <p class="lead">Daftarkan diri anda sekarang, gratis!</p>
          </div>
        </div>
        <div class="row top-course">
          <div class="col-sm-2" id="ukuran">
            <a href="<?php echo base_url('auth/registrasisiswa')?>" class="course">
              <img src="<?php echo base_url('assets/')?>images/1.png" alt="Image placeholder">
              <h2>Siswa</h2>
            </a>
          </div><div></div>
          <div class="col-sm-2" id="ukuran1">
            <a href="<?php echo base_url('auth/registrasiguru')?>" class="course">
              <img src="<?php echo base_url('assets/')?>images/2.png" alt="Image placeholder">
              <h2>Instruktur</h2>
            </a>
          </div>
        </div>
      </div>
    </section>



<footer class="site-footer" style="background-image: url(../images/big_image_3.jpg);">