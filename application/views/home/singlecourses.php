
<body>   
  <section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo base_url()?>assets/images/big_image_1.jpg);" id="headerq">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-12">


        <?php foreach($mapel as $data){?>
            <div class="mb-5 element-animate">
              <div class="row align-items-center">
                <div class="col-md-8">
                  <h1 class="mb-0"><?php echo $data['nama'];?></h1>
                  <!-- <p>By Gregg White</p> -->

                  <?php if($data['jenis_pembayaran'] == "BUY") { ?>
                    <p class="lead mb-5"><?php echo "Rp. ".number_format($data['harga'],0,",",".") ?></p>
                  <?php } else { ?>
                    <p class="lead mb-5"><?php echo $data['jenis_pembayaran'] ?></p>
                  <?php } ?>
                  
                  <?php 
                    if($this->session->userdata('userid') != NULL) {
                      if(empty($trans)){ ?>
                        <p><a href="#tambahh" data-toggle="modal" data-dismiss="#tambahh" class="btn btn-primary mr-2">Start Course</a> <!-- a href="#" class="btn btn-outline-white">Add To Watch List</a> --></p>
                      <?php } else { ?>
                        <p><a href="#" class="btn btn-warning mr-2">Sudah Terbeli</a> <!-- a href="#" class="btn btn-outline-white">Add To Watch List</a> --></p>
                      <?php }
                    }
                  ?>
                </div>
                <div class="col-md-4">
                  <img src="<?php echo base_url('upload/foto/mapel/'.$data['foto']) ?>" alt="Image placeholder" class="img-fluid">
                </div>
              </div>
            </div>    
          </div>
        </div>
      </div>
    </section><?php           } ?>
    <!-- END section -->

    
     <section class="site-section episodes">
       <div class="container">
        <?php $num=1; foreach ($materi as $m) { ?>
          <div class="row bg-light align-items-center p-4 episode">
            <div class="col-md-3">
              <span class="episode-number"><?php echo $num; ?></span>
            </div>
            <div class="col-md-9">
              <p class="meta">Episode <?php echo $num; ?></p>
              <h2><a href="#"><?php echo $m['nama_materi']; ?></a></h2>
              <p>Dosen yang Mengajar adalah <?php echo $m['nama_dosen']; ?></p>
            </div>
          </div>
        <?php $num++; } ?>
      </div>
    </section>


   <footer class="site-footer" style="background-image: url(<?php echo base_url()?>assets/images/big_image_3.jpg);">
 
      <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="<?php echo base_url('assets/')?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery-migrate-3.0.0.js"></script>
    <script src="<?php echo base_url('assets/')?>js/popper.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.stellar.min.js"></script>

    
    <script src="<?php echo base_url('assets/')?>js/main.js"></script>
  </body>
