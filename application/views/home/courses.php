<section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(../images/big_image_1.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-8 text-center">

            <div class="mb-5 element-animate">
              <h1>Find A Courses And Keep Learn </h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->
       
<?php
    if(is_array($mapel_limit)){ 
 ?>
<section class="featured-courses vertical-column courses-wrap">
        <div class="container">
            <div class="row mx-m-25">
                <div class="col-12 px-25">

                    <div class="mb-5 element-animate text-center">
                      <h1>Courses</h1>
                       <p class="lead">Kursus yang dapat anda ikuti</p>
                    </div>
                </div>

            <?php foreach($mapel_limit as $data){?>
                <div class="col-12 col-md-6 col-lg-4 px-25">
                    <div class="course-content">
                        <figure class="course-thumbnail">
                          <br>
                            <a href="<?php echo base_url('home/singlecourses/'.$data['id'])?>" class="course"> 
                                <img src="<?php echo base_url('upload/foto/mapel/'.$data['foto']) ?>" class="img-responsive " alt="" style="height: 180px;"></a>
                        </figure><!-- .course-thumbnail -->

                        <div class="course-content-wrap">
                            <div class="entry-title">
                                <a href="<?php echo base_url('home/singlecourses/'.$data['id'])?>" class="course"> 
                                <h3 style="text-align: center;"><?php echo $data['nama']?></h3></a></div>

                            <footer class="entry-footer flex justify-content-between align-items-center">

                                <div class="course-cost">
                                    <?php if($data['jenis_pembayaran'] == "BUY") {
                                        echo "Rp. ".number_format($data['harga'],0,",",".");
                                        } else {
                                            echo $data['jenis_pembayaran'];
                                        } ?> <!-- <span class="price-drop">$68</span> -->
                                </div><!-- .course-cost -->
                                <?php 
                                // var_dump($cek); die();
                                if($this->session->userdata('userid')){
                                if($data['id_like'] != NULL) { ?>
                                <div class="col-md-5">
                                <img src="<?php echo base_url() ?>assets/images/goodgrey.png" width="20%"> <?php echo ($data['suka']==NULL ? '0' : $data['suka']); ?>&nbsp;&nbsp;
                                <img src="<?php echo base_url() ?>assets/images/badgrey.png" width="20%"> <?php echo ($data['tidaksuka']==NULL ? '0' : $data['tidaksuka']); ?>
                                </div>
                                <?php } else { ?>
                                <div class="col-md-5">
                                <a href="<?php echo base_url() ?>index.php/Home/Like/<?php echo $data['id'] ?>"><img src="<?php echo base_url() ?>assets/images/like.png" width="20%"></a> <?php echo ($data['suka']==NULL ? '0' : $data['suka']); ?>&nbsp;&nbsp;
                                <a href="<?php echo base_url() ?>index.php/Home/Dislike/<?php echo $data['id'] ?>"><img src="<?php echo base_url() ?>assets/images/dislike.png" width="20%"></a> <?php echo ($data['tidaksuka']==NULL ? '0' : $data['tidaksuka']); ?>
                                </div>
                               
                             <?php }} ?>
                            </footer><!-- .entry-footer -->
                        </div><!-- .course-content-wrap -->
                    </div><!-- .course-content -->
                </div><!-- .col --><?php           } ?>
                <?php           } ?>
                
                
                
                <div class="col-12 px-25 flex justify-content-center">
                    <a class="btn btn-primary" href="<?php echo base_url('home/allcourses')?> ">view all courses</a>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->                            
    </section><!-- .courses-wrap -->

  <footer class="site-footer" style="background-image: url(../images/big_image_3.jpg);">
    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>


    <script src="<?php echo base_url('assets/')?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/main.js"></script>

  </body>

</html>