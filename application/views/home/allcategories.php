<body>  
    <section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo base_url()?>assets/images/big_image_1.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-8 text-center">

            <div class="mb-5 element-animate">
              <h1>Join The Class </h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->


<section class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2>All Class</h2>
          </div>
        </div><?php
                if(is_array($kelas)){
            ?>

        <div class="row top-course">
        
        <?php foreach($kelas as $data){?>
          <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <a href="<?php echo base_url('home/showcategories/'.$data['id'])?>" class="course">   
              <img src="<?php echo base_url('upload/foto/kelas/'.$data['foto']) ?>" class="img-responsive " alt="" style="width:335px;height: 180px;">
              <h2 style="text-align: center;"><?php echo $data['nama']?></h2>          
            </a>
          </div><?php  } ?>
        </div>
      </div>
    </section><?php           } ?>
    <!-- END section -->

      
          <tr>
           <td>
     </table>       

   
    <footer class="site-footer" style="background-image: url(../images/big_image_3.jpg);">

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="<?php echo base_url('assets/')?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery-migrate-3.0.0.js"></script>
    <script src="<?php echo base_url('assets/')?>js/popper.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.stellar.min.js"></script>

    
    <script src="<?php echo base_url('assets/')?>js/main.js"></script>
  </body>

<!-- Mirrored from technext.github.io/skwela/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 20:58:38 GMT -->
</html>