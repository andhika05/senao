  <body>

  <div class="clearfix"> </div>
  <section id="slider" class="slider-parallax revoslider-wrap clearfix">

            <!--
            #################################
                - Revolution Slider -
            #################################
            -->
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>    
                    
                    <?php foreach($slider as $slider):?>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="" data-saveperformance="off" data-title="next">
                        <img src="<?php echo base_url().'upload/foto/slider/'.$slider['foto']; ?>" alt="" data-bgposition="left top" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right bottom">

                        <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                        data-x="340"
                        data-y="235"
                        data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                        data-speed="800"
                        data-start="1000"
                        data-easing="easeOutQuad"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.1"
                        data-endspeed="1000"
                        data-endeasing="Power4.easeIn" style="z-index: 3; color:#FFF"></div>

                        <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder uppercase"
                        data-x="190"
                        data-y="280"
                        data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                        data-speed="800"
                        data-start="1200"
                        data-easing="easeOutQuad"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.1"
                        data-endspeed="1000"
                        data-endeasing="Power4.easeIn" style="z-index: 3; color:#FFF"><?php echo $slider['judul']?></div>

                        <div class="tp-caption customin ltl tp-resizeme revo-slider-desc-text"
                        data-x="200"
                        data-y="390"
                        data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                        data-speed="800"
                        data-start="1400"
                        data-easing="easeOutQuad"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.1"
                        data-endspeed="1000"
                        data-endeasing="Power4.easeIn" style="z-index: 3; width: 750px; max-width: 750px; white-space: normal; color:#FFF"><?php echo $slider['deskripsi']?></div>
                    </li>
                    <?php endforeach?>   
                </ul>

                </div>
            </div>

            <script type="text/javascript">

                var tpj=jQuery;
                tpj.noConflict();

                tpj(document).ready(function() {

                    var apiRevoSlider = tpj('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:9000,
                        startwidth:1140,
                        startheight:500,
                        hideThumbs:200,

                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:3,

                        navigationType:"none",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",

                        touchenabled:"on",
                        onHoverStop:"on",

                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,


                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[8,7,6,5,4,3,2,1],
                        parallaxDisableOnMobile:"on",


                        keyboardNavigation:"on",

                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,

                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,

                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,

                        shadow:0,
                        fullWidth:"off",
                        fullScreen:"off",

                        spinner:"spinner0",

                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,

                        shuffle:"off",


                        forceFullWidth:"off",
                        fullScreenAlignForce:"off",
                        minFullScreenHeight:"400",

                        hideThumbsOnMobile:"on",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"on",
                        hideArrowsOnMobile:"on",
                        hideThumbsUnderResolution:0,

                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        fullScreenOffsetContainer: ".header"
                    });

                }); //ready
            </script>

            <!-- END REVOLUTION SLIDER -->
 </section>

    <section class="site-section">
      <div class="container">
        <section class="school-features text-dark d-flex">
          <div class="inner">
            <div class="media d-block feature">
              <div class="icon"><span class="flaticon-video-call"></span></div>
              <div class="media-body">
                <h3 class="mt-0">Online trainings from experts</h3>
                <p>Dosen dosen yang ahli dibidangnya</p>
              </div>
            </div>

            <div class="media d-block feature">
              <div class="icon"><span class="flaticon-student"></span></div>
              <div class="media-body">
                <h3 class="mt-0">Learn anywhere in the world</h3>
                <p>Belajar di mana saja</p>
              </div>
            </div>

            <div class="media d-block feature">
              <div class="icon"><span class="flaticon-video-player-1"></span></div>
              <div class="media-body">
                <h3 class="mt-0">Creative learning video</h3>
                <p>Modul video</p>
              </div>
            </div>


            <div class="media d-block feature">
              <div class="icon"><span class="flaticon-interface"></span></div>
              <div class="media-body">
                <h3 class="mt-0">Competence test</h3>
                <p>upload hasil tes</p>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>
    <!-- END section -->

    <section class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2>Top Courses</h2>
            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum magnam illum maiores adipisci pariatur, eveniet.</p>
          </div>
        </div>
        <div class="row top-course">
          <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <a href="#" class="course">
              <img src="<?php echo base_url('assets/')?>images/webdesign.jpg" alt="Image placeholder">
              <h2>Web Design 101</h2>
              <p>Enroll Now</p>
            </a>
          </div>
          <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <a href="#" class="course">
              <img src="<?php echo base_url('assets/')?>images/wordpress.jpg" alt="Image placeholder">
              <h2>Learn How To Develop WordPress Plugin</h2>
              <p>Enroll Now</p>
            </a>
          </div>
          <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <a href="#" class="course">
              <img src="<?php echo base_url('assets/')?>images/javascript.jpg" alt="Image placeholder">
              <h2>JavaScript 101</h2>
              <p>Enroll Now</p>
            </a>
          </div>
          <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <a href="#" class="course">
              <img src="<?php echo base_url('assets/')?>images/photoshop.jpg" alt="Image placeholder">
              <h2>Photoshop Design 101</h2>
              <p>Enroll Now</p>
            </a>
          </div>
          <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <a href="#" class="course">
              <img src="<?php echo base_url('assets/')?>images/reactjs.jpg" alt="Image placeholder">
              <h2>Learn Native ReactJS</h2>
              <p>Enroll Now</p>
            </a>
          </div>
          <div class="col-lg-2 col-md-4 col-sm-6 col-12">
            <a href="#" class="course">
              <img src="<?php echo base_url('assets/')?>images/angularjs.jpg" alt="Image placeholder">
              <h2>Learn AngularJS 2</h2>
              <p>Enroll Now</p>
            </a>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->
  <footer class="site-footer" style="background-image: url(images/big_image_3.jpg);">
    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="<?php echo base_url('assets/')?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/popper.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.stellar.min.js"></script>

    <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
    
    <script src="<?php echo base_url('assets/')?>js/main.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery-migrate-3.0.0.js"></script>


  </body>

<!-- Mirrored from technext.github.io/skwela/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 20:58:19 GMT -->
</html>