<div class="clearfix"></div>
<section id="slider" class="slider-parallax revoslider-wrap clearfix">
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>    
                    
                    <?php foreach($slider as $slid):?>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="" data-saveperformance="on" data-title="next">
                        <img src="<?php echo base_url().'upload/foto/slider/'.$slid['foto']; ?>" alt="" data-bgposition="left top" data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="100%" data-bgfitend="100%" data-bgpositionend="right bottom">

                        <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                        data-x="340"
                        data-y="235"
                        data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                        data-speed="800"
                        data-start="1000"
                        data-easing="easeOutQuad"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.1"
                        data-endspeed="1000"
                        data-endeasing="Power4.easeIn" style="z-index: 3; color:#FFF"></div>

                        <!-- <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder uppercase"
                        data-x="490"
                        data-y="180"
                        data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                        data-speed="800"
                        data-start="1200"
                        data-easing="easeOutQuad"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.1"
                        data-endspeed="1000"
                        data-endeasing="Power4.easeIn" style="z-index: 3; color:#000"><?php echo $slid['judul']?></div> -->

                        <div class="tp-caption customin ltl tp-resizeme revo-slider-desc-text"
                        data-x="250"
                        data-y="400"
                        data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                        data-speed="800"
                        data-start="1400"
                        data-easing="easeOutQuad"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.1"
                        data-endspeed="1000"
                        data-endeasing="Power4.easeIn" style="z-index: 3; width: 750px; max-width: 750px; white-space: normal; color:#FFF">
                        <?php echo $slid['deskripsi']?></div>
                    </li>
                    <?php endforeach?>      
                </ul>
                </div>
            </div>
 </section>

   <div class="clearfix"></div>
    <section class="site-section" style="margin:-8% 10%;">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2>What we get</h2>
            <p class="lead">Senao brings a lot of benefits</p>
          </div>
        </div>
        <section class="school-features text-dark d-flex">
          <div class="inner">
            <div class="media d-block feature">
              <div class="icon" style="margin-left: 25%;"><span class="flaticon-video-call"></span></div>
              <div class="media-body">
                <h3 class="mt-0">Online trainings from experts</h3>
                <p>The quality instructors here are guaranteed</p>
              </div>
            </div>

            <div class="media d-block feature">
              <div class="icon" style="margin-left: 25%;"><span class="flaticon-student"></span></div>
              <div class="media-body">
                <h3 class="mt-0">Learn anywhere in the world</h3>
                <p>You can study fun anywhere at any time</p>
              </div>
            </div>

            <div class="media d-block feature">
              <div class="icon" style="margin-left: 25%;"><span class="flaticon-video-player-1"></span></div>
              <div class="media-body">
                <h3 class="mt-0">Creative learning video</h3>
                <p>Video modules make learning easier</p>
              </div>
            </div>


            <div class="media d-block feature">
              <div class="icon"style="margin-left: 25%;"><span class="flaticon-interface"></span></div>
              <div class="media-body">
                <h3 class="mt-0">Competence test</h3>
                <p>Upload your test results complete all progress and get a certificate</p>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>
    <!-- END section -->

    <!-- top course-->
    <section class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2>Top Courses</h2>
            <p class="lead">Top courses recommend for you</p>
          </div>
        </div>
       <div class="row top-course">
          <?php foreach($mapel_top as $top) { ?>
          <div class="col-lg-4 col-md-4 col-sm-4 col-4">
            <a href="#" class="course">
              <img src="<?php echo base_url('upload/foto/mapel/'.$top['foto']) ?>" alt="Image placeholder" style="height: 180px;">
              <h2><?php echo $top['nama']?></h2>
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                  <p><?php if($top['jenis_pembayaran'] == "BUY") {
                                        echo "Rp. ".number_format($top['harga'],0,",",".");
                                        } else {
                                            echo $top['jenis_pembayaran'];
                                        } ?></p>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-8">
                  <img src="<?php echo base_url() ?>assets/images/goodgrey.png" width="10%"> <?php echo ($top['suka']==NULL ? '0' : $top['suka']); ?>&nbsp;&nbsp;
                  <img src="<?php echo base_url() ?>assets/images/badgrey.png" width="10%"> <?php echo ($top['tidaksuka']==NULL ? '0' : $top['tidaksuka']); ?>
                </div>
              </div>
            </a>
          </div>
          <?php } ?>
        </div>
      </div>
    </section>

    <!--testimoni-->
     <div id="carousel">
  <div class="btn-bar">
    <div id="buttons">
        <a id="prev" href="#" style="margin-left: -10%"><</a>
        <a id="next" href="#">></a> 
    </div>
  </div>
    <div id="slides"> 
            <ul>
                <?php foreach ($testimoni as $data) { 
                    if($data['testimoni']!=NULL){?>
                <li class="slide">
                    <div class="quoteContainer">
                        <p class="quote-phrase"><span class="quote-marks">"</span><?php echo $data['testimoni']?><class="quote-marks">"</span>
                        </p>
                    </div>
                    <div class="authorContainer">
                        <p class="quote-author"><?php echo $data['nama_siswa']?></p>
                    </div>
                </li>
                <?php }} ?>
            </ul>
       </div>     </class="quote-marks">


</div>
    
     <script type="text/javascript">

                var tpj=jQuery;
                tpj.noConflict();

                tpj(document).ready(function() {

                    var apiRevoSlider = tpj('.tp-banner').show().revolution(
                    {
                        dottedOverlay:"none",
                        delay:9000,
                        startwidth:1140,
                        startheight:700,
                        hideThumbs:200,

                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:3,

                        navigationType:"none",
                        navigationArrows:"solo",
                        navigationStyle:"preview2",

                        touchenabled:"on",
                        onHoverStop:"on",

                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,


                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[8,7,6,5,4,3,2,1],
                        parallaxDisableOnMobile:"on",


                        keyboardNavigation:"on",

                        navigationHAlign:"center",
                        navigationVAlign:"bottom",
                        navigationHOffset:0,
                        navigationVOffset:20,

                        soloArrowLeftHalign:"left",
                        soloArrowLeftValign:"center",
                        soloArrowLeftHOffset:20,
                        soloArrowLeftVOffset:0,

                        soloArrowRightHalign:"right",
                        soloArrowRightValign:"center",
                        soloArrowRightHOffset:20,
                        soloArrowRightVOffset:0,

                        shadow:0,
                        fullWidth:"off",
                        fullScreen:"off",

                        spinner:"spinner0",

                        stopLoop:"off",
                        stopAfterLoops:-1,
                        stopAtSlide:-1,

                        shuffle:"off",


                        forceFullWidth:"off",
                        fullScreenAlignForce:"off",
                        minFullScreenHeight:"400",

                        hideThumbsOnMobile:"on",
                        hideNavDelayOnMobile:1500,
                        hideBulletsOnMobile:"on",
                        hideArrowsOnMobile:"on",
                        hideThumbsUnderResolution:0,

                        hideSliderAtLimit:0,
                        hideCaptionAtLimit:0,
                        hideAllCaptionAtLilmit:0,
                        startWithSlide:0,
                        fullScreenOffsetContainer: ".header"
                    });

                }); //ready
            </script>
            <script src="http://code.jquery.com/jquery-1.11.0.min.js"> </script>
            <script>
                  $(document).ready(function () {
                //rotation speed and timer
                var speed = 5000;
                
                var run = setInterval(rotate, speed);
                var slides = $('.slide');
                var container = $('#slides ul');
                var elm = container.find(':first-child').prop("tagName");
                var item_width = container.width();
                var previous = 'prev'; //id of previous button
                var next = 'next'; //id of next button
                slides.width(item_width); //set the slides to the correct pixel width
                container.parent().width(item_width);
                container.width(slides.length * item_width); //set the slides container to the correct total width
                container.find(elm + ':first').before(container.find(elm + ':last'));
                resetSlides();
                
                
                //if user clicked on prev button
                
                $('#buttons a').click(function (e) {
                    //slide the item
                    
                    if (container.is(':animated')) {
                        return false;
                    }
                    if (e.target.id == previous) {
                        container.stop().animate({
                            'left': 0
                        }, 1500, function () {
                            container.find(elm + ':first').before(container.find(elm + ':last'));
                            resetSlides();
                        });
                    }
                    
                    if (e.target.id == next) {
                        container.stop().animate({
                            'left': item_width * -2
                        }, 1500, function () {
                            container.find(elm + ':last').after(container.find(elm + ':first'));
                            resetSlides();
                        });
                    }
                    
                    //cancel the link behavior            
                    return false;
                    
                });
                
                //if mouse hover, pause the auto rotation, otherwise rotate it    
                container.parent().mouseenter(function () {
                    clearInterval(run);
                }).mouseleave(function () {
                    run = setInterval(rotate, speed);
                });
                
                
                function resetSlides() {
                    //and adjust the container so current is in the frame
                    container.css({
                        'left': -1 * item_width
                    });
                }
                
            });
            //a simple function to click next link
            //a timer will call this function, and the rotation will begin

            function rotate() {
                $('#next').click();
            }
           

            </script>

    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

  </body>

    <!-- END section -->
  <footer class="site-footer" style="background-image: url(images/big_image_3.jpg);">