<!doctype html>
<html lang="en">
  
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <title>Colorlib Listed Directory Template</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,900" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/owl.carousel.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/')?>fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>fonts/flaticon/font/flaticon.css">

    <!-- Theme Style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/style.css">
  </head>
  <body>
    <section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(../images/big_image_1.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-8 text-center">

            <div class="mb-5 element-animate">
              <h1>Login</h1>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->


   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Lupa Kata Kunci</h4>
                </div>
                <form name="sendmail" id="sendmail" action="index.php?p=forpass" method="post" role="form">    
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Email:</label>
                            <input type="mail" class="form-control" id="forgetmail" name="forgetmail" placeholder="Masukkan Email">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Nama Pengguna:</label>
                            <input type="text" class="form-control" id="forgetusername" name="forgetusername" placeholder="Masukkan Nama Pengguna">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Send message" name="submitemail">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <section class="form-log">
        <div class="container">
            <div class="col-md-4" style="
                display:block; margin-left: auto; margin-right: auto;
            ">
        
                
                <form name="login" id="login" class="form-group form-login" role="form" action="<?php echo base_url() ?>auth/domasuk" method="post" style="
                ">
                    <?php
                        if($this->session->flashdata("error") != ""){
                             echo "<label class='label label-danger' style='color:red;'>".$this->session->flashdata("error")."</label>";
                        }
                        if($this->session->flashdata("message") != ""){
                             echo "<p class='label label-warning text-center'>".$this->session->flashdata("message")."</p>";
                        }
                    ?><br>
                            
                    <label for="namalogin" class="control-label"> Nama </label>
                    <input type="text" name="namalogin" class="form-control" id="namalogin" value="" style=" width: 400px;" >
                    <label for="kuncilogin" class="control-label"> Kata Kunci </label>
                    <input type="password" name="kuncilogin" class="form-control" id="kuncilogin" value="" style=" width: 400px;" >
                    <input type="submit" name="finish_reg" value="Log in" class="btn btn-primary">
                    <!-- <p>Lupa Kata Kunci? Masuk 
                        <a href="#" type="button" data-toggle="modal" data-target="#exampleModal"> Disini</a>
                    </p> -->
                </form>
            </div>
        </div>
    </section>
    <!-- END section -->  
     <footer class="site-footer" style="background-image: url(../images/big_image_3.jpg);">

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="<?php echo base_url('assets/')?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery-migrate-3.0.0.js"></script>
    <script src="<?php echo base_url('assets/')?>js/popper.min.js"></script>
    <script src="<?php echo base_url('assets/')?>s/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.stellar.min.js"></script>

    
    <script src="<?php echo base_url('assets/')?>js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<script type="text/javascript">

    <?php if($this->session->flashdata('error')){  ?>
        toastr.error("<?php echo $this->session->flashdata('error'); ?>");
    <?php } ?>
</script>
  </body>

<!-- Mirrored from technext.github.io/skwela/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 20:58:38 GMT -->
</html>