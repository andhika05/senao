<!doctype html>
<html lang="en">
  
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <title>Gloed</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/owl.carousel.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/')?>fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/responsive.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url().'assets/'; ?>css/flexslider.css" type="text/css" media="screen" />


    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/elegant-fonts.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/swiper.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/stylecourses.css">

    <!-- Theme Style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/style.css">

    <!-- External JavaScripts
    ============================================= -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/include/rs-plugin/css/settings.css" media="screen" />

    <!-- TESTIMONI -->
   
</head>


    <style>
        .revo-slider-emphasis-text {
            font-size: 70px;
            font-weight: 700;
            letter-spacing: 0px;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }

        .revo-slider-desc-text {
            font-size: 26px;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }

        .revo-slider-caps-text {
            font-size: 30px;
            font-weight: 400;
        }
    
    .tparrows.preview2 .tp-arr-titleholder {
      text-transform: uppercase;
      font-weight:bold;
    }

      #carousel {
      position: relative;
      width:60%;
      margin:0 auto;
      }

      #slides {
      overflow: hidden;
      position: relative;
      width: 100%;
      height: 250px;
      }

      #slides ul {
      list-style: none;
      width:100%;
      height:250px;
      margin: 0;
      padding: 0;
      position: relative;
      }

       #slides li {
      width:100%;
      height:250px;
      float:left;
      text-align: center;
      position: relative;
      font-family:lato, sans-serif;
      }
      /* Styling for prev and next buttons */
      .btn-bar{
          max-width: 346px;
          margin: 0 auto;
          display: block;
          position: relative;
          top: 40px;
          width: 100%;
      }

       #buttons {
      padding:0 0 5px 0;
      float:right;
      }

      #buttons a {
      text-align:center;
      display:block;
      font-size:50px;
      float:left;
      outline:0;
      margin:0 60px;
      color:#b14943;
      text-decoration:none;
      display:block;
      padding:9px;
      width:35px;
      }

      a#prev:hover, a#next:hover {
      color:#FFF;
      text-shadow:.5px 0px #b14943;  
      }

      .quote-phrase, .quote-author {
      font-family:sans-serif;
      font-weight:300;
      display: table-cell;
      vertical-align: middle;
      padding: 5px 20px;
      font-family:'Lato', Calibri, Arial, sans-serif;
      }

      .quote-phrase {
      height: 200px;
      font-size:24px;
      color:#05347b;
      font-style:italic; 
      }

      .quote-marks {
      font-size:30px;
      padding:0 3px 3px;
      position:inherit;
      }

      .quote-author {
      font-style:normal;
      font-size:20px;
      color:#b14943;
      font-weight:400;
      height: 30px;
      }

      .quoteContainer, .authorContainer {
      display: table;
      width: 100%;
      }

    </style>
  </head>


   <header role="banner" style="background-color: #012f61;">
      <nav class="navbar navbar-expand-md navbar-dark bg-light">
        <div class="container">
          <a class="navbar-brand absolute" href="<?php echo base_url('home')?>">SENAO</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
            <ul class="navbar-nav mx-auto">
<!--          <li class="nav-item">
                <?php $halaman    = strtolower($this->router->fetch_method());?>
                  <li class="<?php echo ($halaman == 'index') ? 'active' : '' ?>">
                    <a href="<?php echo base_url('home') ?>">Home</a>
                      <span class="<?php echo ($halaman == 'index') ? 'home' : '' ?>"></span>
                  </li>
              </li> -->
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('home/courses')?>"">Courses</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('home/categories')?>"">Categories</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('home/about')?>"">About</a>
              </li>

              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('home/contact')?>"">Contact</a>
              </li>
            </ul>

            
            <ul class="navbar-nav absolute-right">
                <?php if($this->session->userdata('level')==2){ ?>
                <li class="nav-item">
                  <a href="<?php echo base_url('siswa/profil')?>" class="nav-link active">Halaman Admin Siswa </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url('auth/keluar')?>" class="nav-link active">Keluar</a>
                </li>
                <?php } else { ?>
                <li class="nav-item">
                  <a href="<?php echo base_url('auth/login')?>" class="nav-link active">Login</a>
                </li>
               
               <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url('home/register')?>"">Register</a>
                </li>
            </ul>
    
 
        <?php } ?>
              
          </div>
        </div>
      </nav>
    </header><div class="clearfix"> </div><br><br><br>
    <!-- END header -->
