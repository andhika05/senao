
      <div class="container" >
        <div class="row mb-5">
          <div class="col-md-4">
            <h3>About</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi, accusantium optio unde perferendis eum illum voluptatibus dolore tempora, consequatur minus asperiores temporibus reprehenderit.</p>
          </div>
          <div class="col-md-6 ml-auto" style="margin-left:20%;">
            

            <div class="row">
              <div class="col-md-5" style="text-align: center; margin-left: 75%;">
                <ul class="list-unstyled">
                  <li><a class="" href="<?php echo base_url('home/courses')?>"">Courses</a></li>
                  <li><a class="" href="<?php echo base_url('home/categories')?>"">Categories</a></li>
                  <li><a class="" href="<?php echo base_url('home/about')?>"">About</a></li>
                  <li><a class="" href="<?php echo base_url('home/contact')?>"">Contact</a></li>
                  <li><a class="" href="<?php echo base_url('auth/login')?>">Login</a></li>
                  <li><a class="" href="<?php echo base_url('home/register')?>"">Register</a></li>
                </ul>
              </div>
            </div>


    <div class="clearfix"></div>
          </div>
        </div>
        <div class="col-md-12 w3lscopyrightaitsgrid w3lscopyrightaitsgrid2" style="text-align: right; margin-left:4.5%">
      <div class="agilesocialwthree">
        <ul class="social-icons">
          <li><a href="#" class="facebook w3ls" title="Go to Our Facebook Page"><i class="fa w3ls fa-facebook-square" aria-hidden="true"></i></a></li>
          <li><a href="#" class="twitter w3l" title="Go to Our Twitter Account"><i class="fa w3l fa-twitter-square" aria-hidden="true"></i></a></li>
          <li><a href="#" class="googleplus w3" title="Go to Our Google Plus Account"><i class="fa w3 fa-google-plus-square" aria-hidden="true"></i></a></li>
          <li><a href="#" class="instagram wthree" title="Go to Our Instagram Account"><i class="fa wthree fa-instagram" aria-hidden="true"></i></a></li>
          <li><a href="#" class="youtube w3layouts" title="Go to Our Youtube Channel"><i class="fa w3layouts fa-youtube-square" aria-hidden="true"></i></a></li>
        </ul>
      </div>
    </div>

        <div class="row">
          <div class="col-md-12">
            <p>Made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com/" target="_blank">Colorlib</a> &copy;<script>document.write(new Date().getFullYear());</script> </p>
          </div>

      
        </div>
      </div>
    </footer>

        <?php 
            if (is_array($modal)) {
                foreach ($modal as $modal){
                    echo $modal;
                }
            }
            if (is_array($js)) {
                    foreach ($js as $js){
                        ?>
                        <script type='text/javascript' src='<?php echo base_url("../assets/js/").$js; ?>'></script>
                        <?php
                    }
                }?>
                <?php if (is_array($validasi)) {
                    foreach ($validasi as $validasi){
                        echo $validasi;
                    }
                }?>
    <!-- END footer -->

    <script type="text/javascript">
      $('#exampleModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var recipient = button.data('whatever') // Extract info from data-* attributes
      var modal = $(this)
      modal.find('.name input').val(recipient)
    })
  </script>



    <!-- TAK PINDAH DARI INDEX -->
    <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery-migrate-3.0.0.js"></script>

    <script src="<?php echo base_url('assets/')?>js/popper.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/')?>js/jquery.stellar.min.js"></script>

    
    <!-- COURSES-->
    <script src='<?php echo base_url('assets/')?>js/jquery.js'></script>
    <script src='<?php echo base_url('assets/')?>js/swiper.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/masonry.pkgd.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/jquery.collapsible.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/custom.js'></script>


    <script src="<?php echo base_url()?>assets/js/main.js"></script>          

  
