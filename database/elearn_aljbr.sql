-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 04, 2019 at 03:10 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearn_aljbr`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `description` text NOT NULL,
  `username` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `time`, `description`, `username`) VALUES
(1, '2019-05-19 13:16:47', 'logout dari sistem', 'jabar'),
(2, '2019-05-19 13:17:01', 'login ke sistem', 'superadmin'),
(3, '2019-05-19 13:18:29', 'menambahkan mata_pelajaran Elektromagnetik', 'superadmin'),
(4, '2019-05-19 13:18:42', 'menambahkan dosen ID #6 ke mata pelajaran ID #1', 'superadmin'),
(5, '2019-05-19 13:20:11', 'login ke sistem', 'dosenelektro'),
(6, '2019-05-19 13:20:42', 'menambahkan materi Bab 1 ke mata kuliah ID #1', 'dosenelektro'),
(7, '2019-05-19 13:20:42', 'menambahkan submateri Pendahuluan ke materi Bab 1', 'dosenelektro'),
(8, '2019-05-19 13:24:14', 'menambahkan konten materi class untuk submateri 1', 'dosenelektro'),
(9, '2019-05-19 13:24:56', 'menambahkan materi Bab 2 ke mata kuliah ID #1', 'dosenelektro'),
(10, '2019-05-19 13:24:56', 'menambahkan submateri Latar Belakang ke materi Bab 2', 'dosenelektro'),
(11, '2019-05-19 13:25:27', 'menambahkan konten materi class untuk submateri 2', 'dosenelektro'),
(12, '2019-05-19 13:25:57', 'login ke sistem', 'jabar'),
(13, '2019-05-19 13:26:37', 'logout dari sistem', 'jabar'),
(14, '2019-05-19 13:26:50', 'login ke sistem', 'superadmin'),
(15, '2019-05-19 13:27:25', 'login ke sistem', 'jabar'),
(16, '2019-05-19 13:29:40', 'login ke sistem', 'dosenelektro'),
(17, '2019-05-19 13:30:40', 'mengupload tugas classuntuk submateri 1)', 'jabar'),
(18, '2019-05-19 13:30:40', 'mengupload tugas classuntuk submateri 1)', 'jabar'),
(19, '2019-05-19 13:32:05', 'logout dari sistem', 'jabar'),
(20, '2019-05-19 13:32:16', 'login ke sistem', 'jabar'),
(21, '2019-05-19 13:38:01', 'menambahkan materi Bab 3 ke mata kuliah ID #1', 'dosenelektro'),
(22, '2019-05-19 13:38:01', 'menambahkan submateri Rumusan Masalah ke materi Bab 3', 'dosenelektro'),
(23, '2019-05-19 13:38:27', 'menambahkan konten materi class untuk submateri 3', 'dosenelektro'),
(24, '2019-06-15 19:49:56', 'login ke sistem', 'dosenelektro'),
(25, '2019-06-15 20:02:38', 'login ke sistem', 'alibasanta'),
(26, '2019-06-15 20:05:23', 'login ke sistem', 'superadmin'),
(27, '2019-06-15 20:07:15', 'menambahkan mata_pelajaran Kewirausahaan Listrik', 'superadmin'),
(28, '2019-06-15 20:07:36', 'menambahkan dosen ID #1 ke mata pelajaran ID #2', 'superadmin'),
(29, '2019-06-15 20:08:56', 'menambahkan mata_pelajaran Neraca keuangan', 'superadmin'),
(30, '2019-06-15 20:09:07', 'menambahkan dosen ID #1 ke mata pelajaran ID #3', 'superadmin'),
(31, '2019-06-15 20:10:36', 'login ke sistem', 'ibnu1993'),
(32, '2019-06-15 20:12:13', 'menambahkan submateri Demand Pasar ke materi Bab 1', 'ibnu1993'),
(33, '2019-06-15 20:12:44', 'menambahkan submateri Demand Pasar ke materi Bab 1', 'ibnu1993'),
(34, '2019-06-15 20:13:04', 'menambahkan materi Bab 7 ke mata kuliah ID #3', 'ibnu1993'),
(35, '2019-06-15 20:13:05', 'menambahkan submateri Demand Pasar ke materi Bab 7', 'ibnu1993'),
(36, '2019-06-15 20:52:08', 'login ke sistem', 'jabar'),
(37, '2019-06-15 20:55:51', 'logout dari sistem', 'jabar'),
(38, '2019-06-15 20:56:16', 'login ke sistem', 'dosenelektro'),
(39, '2019-06-15 20:58:16', 'menambahkan materi Bab 4 ke mata kuliah ID #1', 'dosenelektro'),
(40, '2019-06-15 20:58:16', 'menambahkan submateri Listrik Pikachu ke materi Bab 4', 'dosenelektro'),
(41, '2019-06-15 20:58:45', 'menambahkan konten materi class untuk submateri 7', 'dosenelektro'),
(42, '2019-06-15 21:01:23', 'login ke sistem', 'superadmin'),
(43, '2019-06-16 14:13:29', 'login ke sistem', 'ibnu1993'),
(44, '2019-06-16 14:14:29', 'login ke sistem', 'superadmin'),
(45, '2019-06-16 14:24:12', 'mendaftar sebagai guru', 'dosen_ak'),
(46, '2019-06-16 14:24:25', 'login ke sistem', 'dosen_ak'),
(47, '2019-06-16 14:28:12', 'login ke sistem', 'superadmin'),
(48, '2019-06-16 14:29:25', 'menambahkan dosen ID #8 ke mata pelajaran ID #3', 'superadmin'),
(49, '2019-06-16 14:29:27', 'menghapus dosen ID #1 dari mata pelajaran ID #3', 'superadmin'),
(50, '2019-06-16 14:29:34', 'menghapus mata pelajaran ID #2', 'superadmin'),
(51, '2019-06-16 14:29:36', 'menghapus mata pelajaran ID #2', 'superadmin'),
(52, '2019-06-16 14:29:40', 'menghapus mata pelajaran ID #2', 'superadmin'),
(53, '2019-06-16 14:29:41', 'menghapus mata pelajaran ID #2', 'superadmin'),
(54, '2019-06-16 14:29:54', 'menambahkan dosen ID #1 ke mata pelajaran ID #2', 'superadmin'),
(55, '2019-06-16 14:29:55', 'menghapus mata pelajaran ID #2', 'superadmin'),
(56, '2019-06-16 14:29:57', 'menghapus mata pelajaran ID #2', 'superadmin'),
(57, '2019-06-16 14:29:58', 'menghapus mata pelajaran ID #2', 'superadmin'),
(58, '2019-06-16 14:29:59', 'menghapus mata pelajaran ID #2', 'superadmin'),
(59, '2019-06-16 14:29:59', 'menghapus mata pelajaran ID #2', 'superadmin'),
(60, '2019-06-16 14:35:22', 'login ke sistem', 'dosen_ak'),
(61, '2019-06-16 14:36:04', 'menambahkan submateri Uang is money ke materi Bab 1', 'dosen_ak'),
(62, '2019-06-16 14:36:47', 'menambahkan materi Bab 1. Uang Is Money ke mata kuliah ID #4', 'dosen_ak'),
(63, '2019-06-16 14:36:47', 'menambahkan submateri Deskripsi uang ke materi Bab 1. Uang Is Money', 'dosen_ak'),
(64, '2019-06-16 14:38:49', 'menambahkan konten materi class untuk submateri 9', 'dosen_ak'),
(65, '2019-06-16 14:51:47', 'login ke sistem', 'siswa1'),
(66, '2019-06-16 15:18:25', 'login ke sistem', 'siswa1'),
(67, '2019-06-23 14:02:58', 'login ke sistem', 'jabar'),
(68, '2019-06-23 14:11:57', 'logout dari sistem', 'jabar'),
(69, '2019-06-23 14:53:33', 'login ke sistem', 'jabar'),
(70, '2019-06-23 15:26:39', 'logout dari sistem', 'jabar'),
(71, '2019-06-23 15:26:56', 'login ke sistem', 'dosenelektro'),
(72, '2019-06-25 19:29:12', 'login ke sistem', 'jabar'),
(73, '2019-06-25 19:29:15', 'logout dari sistem', 'jabar'),
(74, '2019-06-25 19:30:23', 'login ke sistem', 'superadmin'),
(75, '2019-06-25 19:31:09', 'menghapus mata pelajaran ID #3', 'superadmin'),
(76, '2019-06-25 19:31:20', 'menghapus dosen ID #8', 'superadmin'),
(77, '2019-06-25 19:31:37', 'mengupdate kelas untuk kelas ID #16', 'superadmin'),
(78, '2019-06-25 19:34:01', 'menambahkan mata_pelajaran Kimia', 'superadmin'),
(79, '2019-06-25 19:39:10', 'menghapus mata pelajaran ID #1', 'superadmin'),
(80, '2019-06-25 19:39:11', 'menghapus mata pelajaran ID #4', 'superadmin'),
(81, '2019-06-25 19:52:48', 'menambahkan mata_pelajaran Senyawa Kimia', 'superadmin'),
(82, '2019-06-25 20:17:35', 'login ke sistem', 'siswa1'),
(83, '2019-06-26 20:01:03', 'login ke sistem', 'superadmin'),
(84, '2019-06-26 20:07:48', 'mendaftar sebagai guru', 'dosenkimia'),
(85, '2019-06-26 20:07:59', 'login ke sistem', 'dosenkimia'),
(86, '2019-06-26 20:08:59', 'login ke sistem', 'superadmin'),
(87, '2019-06-26 20:09:13', 'menambahkan dosen ID #9 ke mata pelajaran ID #5', 'superadmin'),
(88, '2019-06-26 20:09:40', 'login ke sistem', 'dosenkimia'),
(89, '2019-06-26 20:09:59', 'login ke sistem', 'jabar'),
(90, '2019-06-26 20:10:55', 'logout dari sistem', 'jabar'),
(91, '2019-06-26 20:11:04', 'login ke sistem', 'dosenkimia'),
(92, '2019-06-26 20:14:55', 'menambahkan materi Bab 1 Pengenalan Senyawa Kimia ke mata kuliah ID #6', 'dosenkimia'),
(93, '2019-06-26 20:14:55', 'menambahkan submateri  ke materi Bab 1 Pengenalan Senyawa Kimia', 'dosenkimia'),
(94, '2019-06-26 20:15:04', 'mengubah materi ID #7 dengan nama baru: Bab 1 Pengenalan Senyawa Kimia', 'dosenkimia'),
(95, '2019-06-26 20:21:58', 'menambahkan konten materi class untuk submateri 10', 'dosenkimia'),
(96, '2019-06-26 20:23:23', 'menghapus konten ID #7', 'dosenkimia'),
(97, '2019-06-26 20:25:09', 'menambahkan materi Bab 1 Pengenalan Senyawa Kimia ke mata kuliah ID #6', 'dosenkimia'),
(98, '2019-06-26 20:25:09', 'menambahkan submateri Tata Nama Senyawa Kimia ke materi Bab 1 Pengenalan Senyawa Kimia', 'dosenkimia'),
(99, '2019-06-26 20:25:31', 'menambahkan konten materi class untuk submateri 11', 'dosenkimia'),
(100, '2019-06-26 20:25:55', 'mengupdate konten materi ID #7 tipe class)', 'dosenkimia'),
(101, '2019-06-26 20:30:31', 'mengupdate konten materi ID #7 tipe class)', 'dosenkimia'),
(102, '2019-06-26 20:33:48', 'mengupdate konten materi ID #7 tipe class)', 'dosenkimia'),
(103, '2019-06-26 20:34:23', 'login ke sistem', 'jabar'),
(104, '2019-06-26 20:45:55', 'login ke sistem', 'dosenkimia'),
(105, '2019-06-26 20:48:44', 'menambahkan materi Rumus Senyawa Kimia ke mata kuliah ID #6', 'dosenkimia'),
(106, '2019-06-26 20:48:45', 'menambahkan submateri Penggunaan Tata Nama Senyawa Kimia ke materi Rumus Senyawa Kimia', 'dosenkimia'),
(107, '2019-06-26 20:49:03', 'menambahkan konten materi class untuk submateri 12', 'dosenkimia'),
(108, '2019-06-26 20:57:50', 'mengupload tugas classuntuk submateri 11)', 'jabar'),
(109, '2019-06-26 20:57:51', 'mengupload tugas classuntuk submateri 11)', 'jabar'),
(110, '2019-06-26 21:13:16', 'login ke sistem', 'superadmin'),
(111, '2019-06-28 19:51:20', 'login ke sistem', 'jabar'),
(112, '2019-06-29 00:39:58', 'login ke sistem', 'jabar'),
(113, '2019-06-29 00:47:08', 'logout dari sistem', 'jabar'),
(114, '2019-06-29 00:47:16', 'login ke sistem', 'superadmin'),
(115, '2019-06-29 01:02:58', 'menambahkan mata_pelajaran Kimia Murni', 'superadmin'),
(116, '2019-06-29 01:04:22', 'login ke sistem', 'jabar'),
(117, '2019-06-29 01:19:48', 'logout dari sistem', 'jabar'),
(118, '2019-06-29 01:20:01', 'login ke sistem', 'dosenkimia'),
(119, '2019-06-29 08:58:48', 'login ke sistem', 'superadmin'),
(120, '2019-06-29 09:07:32', 'login ke sistem', 'dosenkimia'),
(121, '2019-06-29 09:10:41', 'mengupdate konten materi ID #8 tipe class)', 'dosenkimia'),
(122, '2019-06-29 09:11:44', 'mengupdate konten materi ID #8 tipe class)', 'dosenkimia'),
(123, '2019-06-29 09:16:53', 'login ke sistem', 'jabar'),
(124, '2019-06-29 09:20:13', 'mengupload tugas classuntuk submateri 11)', 'jabar'),
(125, '2019-06-29 09:20:13', 'mengupload tugas classuntuk submateri 11)', 'jabar'),
(126, '2019-06-29 09:20:44', 'login ke sistem', 'dosenkimia'),
(127, '2019-06-29 09:31:00', 'logout dari sistem', 'jabar'),
(128, '2019-06-29 09:31:16', 'login ke sistem', 'jabar'),
(129, '2019-06-29 15:44:18', 'login ke sistem', 'superadmin'),
(130, '2019-06-29 15:57:59', 'login ke sistem', 'adminsenao'),
(131, '2019-06-29 16:17:35', 'login ke sistem', 'dosenkimia'),
(132, '2019-06-29 16:22:01', 'login ke sistem', 'dosenkimia'),
(133, '2019-06-29 16:54:47', 'login ke sistem', 'jabar'),
(134, '2019-06-29 17:05:02', 'logout dari sistem', 'jabar'),
(135, '2019-06-29 17:05:09', 'login ke sistem', 'jabar'),
(136, '2019-06-29 17:09:54', 'logout dari sistem', 'jabar'),
(137, '2019-06-29 17:10:14', 'login ke sistem', 'dosenkimia'),
(138, '2019-06-29 17:10:53', 'login ke sistem', 'jabar'),
(139, '2019-06-29 17:18:43', 'logout dari sistem', 'jabar'),
(140, '2019-06-29 17:18:53', 'login ke sistem', 'jabar'),
(141, '2019-06-29 17:27:53', 'mengupload tugas classuntuk submateri 12)', 'jabar'),
(142, '2019-06-29 17:27:53', 'mengupload tugas classuntuk submateri 12)', 'jabar'),
(143, '2019-06-29 17:27:59', 'logout dari sistem', 'jabar'),
(144, '2019-06-29 17:28:09', 'login ke sistem', 'dosenkimia'),
(145, '2019-06-29 17:29:00', 'login ke sistem', 'jabar'),
(146, '2019-06-30 13:18:42', 'login ke sistem', 'jabar'),
(147, '2019-06-30 13:20:40', 'login ke sistem', 'jabar'),
(148, '2019-06-30 14:20:16', 'logout dari sistem', 'jabar'),
(149, '2019-06-30 14:21:16', 'login ke sistem', 'dosenkimia'),
(150, '2019-06-30 15:14:44', 'menambahkan materi Bab Bab Bab ke mata kuliah ID #6', 'dosenkimia'),
(151, '2019-06-30 15:14:44', 'menambahkan submateri Cara Kerja Bab ke materi Bab Bab Bab', 'dosenkimia'),
(152, '2019-06-30 15:15:45', 'menambahkan konten materi class untuk submateri 13', 'dosenkimia'),
(153, '2019-07-04 10:22:53', 'login ke sistem', 'dosenkimia'),
(154, '2019-07-04 10:56:15', 'menambahkan materi Materi A ke mata kuliah ID #6', 'dosenkimia'),
(155, '2019-07-04 10:56:15', 'menambahkan submateri Submateri A ke materi Materi A', 'dosenkimia'),
(156, '2019-07-04 10:57:48', 'menambahkan konten materi class untuk submateri 14', 'dosenkimia'),
(157, '2019-07-04 11:08:17', 'mengupdate konten materi ID #10 tipe class)', 'dosenkimia'),
(158, '2019-07-04 11:09:18', 'menambahkan materi Materi B ke mata kuliah ID #6', 'dosenkimia'),
(159, '2019-07-04 11:09:18', 'menambahkan submateri Submateri B ke materi Materi B', 'dosenkimia'),
(160, '2019-07-04 11:09:53', 'menambahkan konten materi class untuk submateri 15', 'dosenkimia'),
(161, '2019-07-04 11:10:40', 'mengupdate konten materi ID #11 tipe class)', 'dosenkimia'),
(162, '2019-07-04 11:10:41', 'mengupdate konten materi ID #11 tipe class)', 'dosenkimia'),
(163, '2019-07-04 11:15:25', 'login ke sistem', 'jabar'),
(164, '2019-07-04 21:25:32', 'login ke sistem', 'jabar'),
(165, '2019-07-14 21:11:43', 'login ke sistem', 'dosenkimia'),
(166, '2019-07-14 21:13:37', 'mengupdate konten materi ID #7 tipe class)', 'dosenkimia'),
(167, '2019-07-14 21:14:40', 'login ke sistem', 'jabar'),
(168, '2019-07-14 22:14:02', 'login ke sistem', 'jabar'),
(169, '2019-07-14 22:14:18', 'logout dari sistem', 'jabar'),
(170, '2019-07-24 19:59:38', 'login ke sistem', 'dosenelektro'),
(171, '2019-07-24 19:59:52', 'login ke sistem', 'dosenkimia'),
(172, '2019-07-24 20:00:53', 'login ke sistem', 'jabar'),
(173, '2019-07-24 20:07:48', 'logout dari sistem', 'jabar'),
(174, '2019-07-24 20:07:55', 'login ke sistem', 'adminsenao'),
(175, '2019-07-24 20:08:48', 'login ke sistem', 'jabar'),
(176, '2019-07-24 21:28:44', 'logout dari sistem', 'jabar'),
(177, '2019-07-24 21:30:03', 'login ke sistem', 'siswa3'),
(178, '2019-07-24 21:30:47', 'logout dari sistem', 'siswa3'),
(179, '2019-07-26 19:52:55', 'login ke sistem', 'jabar'),
(180, '2019-07-26 20:07:03', 'logout dari sistem', 'jabar'),
(181, '2019-07-26 20:07:15', 'login ke sistem', 'dosenkimia'),
(182, '2019-08-03 20:55:03', 'login ke sistem', 'adminsenao'),
(183, '2019-08-03 20:58:00', 'login ke sistem', 'dosenkimia'),
(184, '2019-08-03 20:59:03', 'mengupdate foto untuk guru ID #9', 'dosenkimia'),
(185, '2019-08-03 21:00:58', 'mengupdate foto untuk guru ID #9', 'dosenkimia'),
(186, '2019-08-03 21:04:28', 'mengupdate konten materi ID #7 tipe class)', 'dosenkimia'),
(187, '2019-08-03 23:01:04', 'mengupdate konten materi ID #7 tipe class)', 'dosenkimia'),
(188, '2019-08-03 23:04:21', 'mengupdate konten materi ID #7 tipe class)', 'dosenkimia'),
(189, '2019-08-03 23:07:01', 'mengubah materi ID #9 dengan nama baru: Bab 2 Rumus Senyawa Kimia', 'dosenkimia'),
(190, '2019-08-03 23:09:26', 'mengupdate konten materi ID #8 tipe class)', 'dosenkimia'),
(191, '2019-08-03 23:10:37', 'mengupdate konten materi ID #8 tipe class)', 'dosenkimia'),
(192, '2019-08-03 23:18:23', 'login ke sistem', 'dosenelektro'),
(193, '2019-08-03 23:18:37', 'mengupdate foto untuk guru ID #6', 'dosenelektro'),
(194, '2019-08-03 23:33:57', 'login ke sistem', 'adminsenao'),
(195, '2019-08-03 23:38:17', 'menambahkan dosen ID #6 ke mata pelajaran ID #6', 'adminsenao'),
(196, '2019-08-03 23:39:20', 'mengupdate mapel untuk mapel ID #6', 'adminsenao'),
(197, '2019-08-03 23:41:29', 'mengupdate mapel untuk mapel ID #6', 'adminsenao'),
(198, '2019-08-03 23:41:40', 'mengupdate mapel untuk mapel ID #6', 'adminsenao'),
(199, '2019-08-03 23:42:43', 'mengupdate mapel untuk mapel ID #5', 'adminsenao'),
(200, '2019-08-03 23:48:37', 'mengupdate mapel untuk mapel ID #6', 'adminsenao'),
(201, '2019-08-03 23:48:59', 'mengupdate mapel untuk mapel ID #6', 'adminsenao'),
(202, '2019-08-03 23:49:08', 'mengupdate mapel untuk mapel ID #6', 'adminsenao'),
(203, '2019-08-03 23:50:01', 'menghapus mata pelajaran ID #6', 'adminsenao'),
(204, '2019-08-03 23:50:30', 'menambahkan mata_pelajaran Teknik Elektro Dasar', 'adminsenao'),
(205, '2019-08-03 23:50:34', 'menambahkan dosen ID #6 ke mata pelajaran ID #7', 'adminsenao'),
(206, '2019-08-03 23:56:24', 'mengupdate slider untuk slider ID #10', 'adminsenao'),
(207, '2019-08-03 23:56:33', 'mengupdate slider untuk slider ID #11', 'adminsenao'),
(208, '2019-08-03 23:56:50', 'menambahkan slider Slider 3Ini adalah Slider 3Slider 3-belajar2.jpg', 'adminsenao'),
(209, '2019-08-03 23:57:02', 'mengupdate slider untuk slider ID #12', 'adminsenao'),
(210, '2019-08-04 00:26:10', 'mengupdate slider untuk slider ID #10', 'adminsenao'),
(211, '2019-08-04 00:39:56', 'menambahkan mata_pelajaran Algoritma Pemrograman', 'adminsenao'),
(212, '2019-08-04 00:42:43', 'mendaftar sebagai guru', 'doseninformatika'),
(213, '2019-08-04 00:43:13', 'login ke sistem', 'adminsenao'),
(214, '2019-08-04 00:43:30', 'menambahkan dosen ID #10 ke mata pelajaran ID #8', 'adminsenao'),
(215, '2019-08-04 12:19:13', 'login ke sistem', 'dosenkimia'),
(216, '2019-08-04 12:21:49', 'login ke sistem', 'dosenelektro'),
(217, '2019-08-04 12:24:05', 'login ke sistem', 'jabar'),
(218, '2019-08-04 12:27:50', 'login ke sistem', 'adminsenao'),
(219, '2019-08-04 12:28:27', 'mengupdate mapel untuk mapel ID #8', 'adminsenao'),
(220, '2019-08-04 12:43:09', 'mengupdate mapel untuk mapel ID #8', 'adminsenao');

-- --------------------------------------------------------

--
-- Table structure for table `data_guru`
--

CREATE TABLE `data_guru` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_guru`
--

INSERT INTO `data_guru` (`id`, `nama`, `nip`, `email`, `foto`) VALUES
(2, 'Super Admin', '0', 'atnasabila@gmail.com', NULL),
(6, 'Dosen Elektro', '1531140120', 'dosenelektro@polinema.ac.id', 'dosenelektro-dosen.png'),
(9, 'Dosen Kimia', '1234567865321', 'dosenkimia@polinema.ac.id', 'dosenkimia-dosenkimiaa.png'),
(10, 'Dosen Informatika', '19845655325311', 'dosenti@polinema.ac.id', 'doseninformatika-dosenti.png');

-- --------------------------------------------------------

--
-- Table structure for table `data_kelas`
--

CREATE TABLE `data_kelas` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `tahun` year(4) NOT NULL,
  `foto` text NOT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_kelas`
--

INSERT INTO `data_kelas` (`id`, `nama`, `tahun`, `foto`, `status`) VALUES
(6, 'Teknologi Informasi', 2018, 'Teknologi_Informasi-ti.jpeg', 1),
(8, 'Elektro', 2018, 'Elektro-elektro.jpg', 1),
(9, 'Teknik Mesin', 2018, 'Teknik_Mesin-mesin.jpg', 1),
(10, 'Teknik Sipil', 2018, 'Teknik_Sipil-sipil1.jpeg', 1),
(13, 'Teknik Kimia', 2018, 'Teknik_Kimia-kimia.jpeg', 1),
(14, 'Administrasi Niaga', 2018, 'Administrasi_Niaga-an.jpeg', 1),
(15, 'Akuntansi', 2018, 'Akuntansi-ak.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_siswa`
--

CREATE TABLE `data_siswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` text NOT NULL,
  `testimoni` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_siswa`
--

INSERT INTO `data_siswa` (`id`, `nama`, `nim`, `email`, `foto`, `testimoni`) VALUES
(4, 'Jabarudinz', '1531140110', 'jabar@gmail.com', 'jabar-101.jpg', 'Sistemnya bagus'),
(8, 'Siswa Satu', '1345635678', 'siswa1@gmail.com', 'siswa1-mbuntu-11.jpg', 'Semoga membuat minat belajar bertambah'),
(9, 'siswa2', '1345635678214', 'siswa2@gmail.com', 'siswa2-mbuntu-10.jpg', ''),
(10, 'siswa3', '181239102131239123', 'siswa3@gmail.com', 'siswa3-mbuntu-11.jpg', 'ASDSADAS');

-- --------------------------------------------------------

--
-- Table structure for table `detail_kelas`
--

CREATE TABLE `detail_kelas` (
  `id` int(11) NOT NULL,
  `kelas_id` int(11) DEFAULT NULL,
  `siswa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_kelas`
--

INSERT INTO `detail_kelas` (`id`, `kelas_id`, `siswa_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3),
(4, 6, 4),
(5, NULL, 5),
(6, 6, 6),
(7, 6, 7),
(8, 8, 8),
(9, 6, 9),
(10, 6, 10);

-- --------------------------------------------------------

--
-- Table structure for table `detail_mapel`
--

CREATE TABLE `detail_mapel` (
  `id` int(11) NOT NULL,
  `t_mapel_id` int(11) NOT NULL,
  `materi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_mapel`
--

INSERT INTO `detail_mapel` (`id`, `t_mapel_id`, `materi_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 3, 4),
(5, 1, 5),
(6, 4, 6),
(7, 6, 7),
(8, 6, 8),
(9, 6, 9),
(10, 6, 10),
(11, 6, 11),
(12, 6, 12);

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `kontenmateri_id` int(11) DEFAULT NULL,
  `subyek` text,
  `deskripsi` text,
  `tanggal` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id`, `user_id`, `level`, `kontenmateri_id`, `subyek`, `deskripsi`, `tanggal`) VALUES
(1, 8, 1, 5, 'Sudah bisa kah?', 'Gimz gez sda pada uplot lom?', '2019-06-16 09:50:51'),
(2, 4, 2, 7, 'Tes', 'Coba nulis komentar', '2019-06-26 16:02:16'),
(3, 9, 1, 7, 'Warning', 'Jangan komentar selain yang berkaitan dengan pelajaran', '2019-06-26 16:05:09'),
(4, 9, 1, 8, 'sadasd', 'sadsadas', '2019-06-29 04:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `kontenmateri`
--

CREATE TABLE `kontenmateri` (
  `id` int(11) NOT NULL,
  `submateri_id` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `pdf` text,
  `video` text,
  `tipe` varchar(5) NOT NULL COMMENT '''class'''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontenmateri`
--

INSERT INTO `kontenmateri` (`id`, `submateri_id`, `keterangan`, `pdf`, `video`, `tipe`) VALUES
(1, 1, '', 'video/1-ARToolKit_v4_4_running_on_iPhone.mp4', '', 'class'),
(2, 2, '', 'pdf/2-buku_ajar_Network_Programming.pdf', '', 'class'),
(3, 3, '', 'video/3-Cara_Mudah_Membuat_Menu_Navigation_Drawer_pada_Android_-_Tutorial_Android_Studio_4.mp4', '', 'class'),
(4, 7, '', 'pdf/7-001-Timeline-PKL.pdf', '', 'class'),
(5, 9, '', 'pdf/9-Pengumuman_Sertifikasi_MikroTIK_2018.pdf', '', 'class'),
(6, 10, '', 'pdf/10-TATA_NAMA_SENYAWA_KIMIA.pdf', '', 'class'),
(7, 11, '<p>Materi ini tentang senyawa kimia</p>', 'pdf/11-10-TATA_NAMA_SENYAWA_KIMIA.pdf', 'video/11-Belajar_Kimia_Tatanama_Senyawa_-_YouTube.mp4', 'class'),
(8, 12, '<p>Materi ini tentang Rumus Senyawa Kimia</p>', 'pdf/12-rumus_senyawa_kimia.pdf', '', 'class');

-- --------------------------------------------------------

--
-- Table structure for table `like_dislike`
--

CREATE TABLE `like_dislike` (
  `id` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `ket` enum('LIKE','DISLIKE') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `like_dislike`
--

INSERT INTO `like_dislike` (`id`, `id_siswa`, `id_mapel`, `ket`) VALUES
(1, 4, 5, 'LIKE'),
(2, 4, 6, 'LIKE');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `level` tinyint(1) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `user_id`, `level`, `status`) VALUES
('ibnu1993', '28222eb36cbc5290d83d03b80569e3e6b6cefc48', 1, 1, 1),
('adminsenao', 'ecafbfbf5a65ccd6481c488a9fa3520ef627f456', 2, 9, 1),
('alibasanta', '3d19c18c95603ed24ef7f683e98ef03abfdeeb2d', 1, 9, 1),
('jabar', '60e9031ddfd3d682e94c91385c6ac947efe104da', 4, 2, 1),
('dosenelektro', '49254ac92bf69db87f2aba79b02a9fcc2d213369', 6, 1, 1),
('aji', '554335427242f68ccfd28dad3189d93dd1ee5ca9', 6, 2, 1),
('siswa_teladan', '28222eb36cbc5290d83d03b80569e3e6b6cefc48', 7, 2, 1),
('siswa1', '28222eb36cbc5290d83d03b80569e3e6b6cefc48', 8, 2, 1),
('siswa2', '28222eb36cbc5290d83d03b80569e3e6b6cefc48', 9, 2, 1),
('siswa3', '28222eb36cbc5290d83d03b80569e3e6b6cefc48', 10, 2, 1),
('hamba', '5bba2b41bf1bf82297c35f671a503a0e93e328d9', 11, 2, 1),
('dosen_ak', '5e8803e959daf1d00459647af266ccb4aeafb4f2', 8, 1, 0),
('dosenkimia', '69a5cb30ef2d8d9d24ac029ed7879f0bb12f8aa8', 9, 1, 1),
('doseninformatika', 'bbf9ddb9774fb1629b6398ad29b8d2730127ff53', 10, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mata_pelajaran`
--

CREATE TABLE `mata_pelajaran` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `foto` text NOT NULL,
  `jenis_pembayaran` enum('FREE','BUY') NOT NULL,
  `harga` int(11) NOT NULL,
  `status` int(11) DEFAULT '1',
  `kelas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_pelajaran`
--

INSERT INTO `mata_pelajaran` (`id`, `nama`, `foto`, `jenis_pembayaran`, `harga`, `status`, `kelas_id`) VALUES
(5, 'Senyawa Kimia', 'Senyawa_Kimia-senyawakim.jpg', 'FREE', 0, 1, 13),
(7, 'Teknik Elektro Dasar', 'Teknik_Elektro_Dasar-aaa.jpg', 'BUY', 50000, 1, 8),
(8, 'Algoritma', 'Algoritma-algoritma.jpg', 'BUY', 30000, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id`, `nama`) VALUES
(8, 'Bab 1 Pengenalan Senyawa Kimia'),
(9, 'Bab 2 Rumus Senyawa Kimia');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id` int(11) NOT NULL,
  `siswa_id` int(11) DEFAULT NULL,
  `submateri_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `nilai_class` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id`, `siswa_id`, `submateri_id`, `class_id`, `nilai_class`) VALUES
(1, 4, 1, NULL, 80),
(2, 4, 11, NULL, 90),
(3, 4, 12, NULL, 80);

-- --------------------------------------------------------

--
-- Table structure for table `progress`
--

CREATE TABLE `progress` (
  `id` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `submateri_id` int(11) NOT NULL,
  `tugas_class` text NOT NULL,
  `tugas_lab` text NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `progress`
--

INSERT INTO `progress` (`id`, `siswa_id`, `submateri_id`, `tugas_class`, `tugas_lab`, `status`) VALUES
(1, 4, 1, 'jabar/1-Hukum_TIK.docx', '', 1),
(4, 4, 2, '', '', 0),
(5, 4, 11, 'jabar/11-11-Kuisioner_Mahasiswa_PKL_MI_20161.doc', '', 1),
(6, 4, 12, 'jabar/12-buku_ajar_Network_Programming.pdf', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `progress_belajar`
--

CREATE TABLE `progress_belajar` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `submateri_id` int(11) NOT NULL,
  `tugas_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id_slider` int(11) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id_slider`, `judul`, `deskripsi`, `foto`, `status`) VALUES
(10, 'Slider 1', 'Ini adalah slider 1', 'Slider_1-belajar3.jpg', 1),
(11, 'Slider 2', 'Ini adalah slider 2', 'Slider_2-belajar1.jpg', 1),
(12, 'Slider 3', 'Ini adalah Slider 3', 'Slider_3-belajar2.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `submateri`
--

CREATE TABLE `submateri` (
  `id` int(11) NOT NULL,
  `materi_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submateri`
--

INSERT INTO `submateri` (`id`, `materi_id`, `nama`) VALUES
(11, 8, 'Tata Nama Senyawa Kimia'),
(12, 9, 'Penggunaan Tata Nama Senyawa Kimia');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `tgl_transaksi` datetime NOT NULL,
  `upload_bukti` varchar(150) DEFAULT NULL,
  `status` enum('Belum Verifikasi','Sudah Verifikasi') NOT NULL,
  `status_data` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_mapel`, `id_siswa`, `tgl_transaksi`, `upload_bukti`, `status`, `status_data`) VALUES
(1, 1, 4, '2019-05-19 01:26:15', '1-apaini.PNG', 'Sudah Verifikasi', 0),
(3, 5, 4, '2019-06-26 08:36:09', '3-9bd67912a00387abbfb3942e7d849315.jpg', 'Sudah Verifikasi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE `tugas` (
  `id` int(11) NOT NULL,
  `file` text,
  `siswa_id` int(11) DEFAULT NULL,
  `kontenmateri_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_jadwal`
--

CREATE TABLE `t_jadwal` (
  `id` int(11) NOT NULL,
  `kelas_id` int(11) DEFAULT NULL,
  `t_mapel_id` int(11) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL,
  `jam` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_mapel`
--

CREATE TABLE `t_mapel` (
  `id` int(11) NOT NULL,
  `mapel_id` int(11) DEFAULT NULL,
  `dosen_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_mapel`
--

INSERT INTO `t_mapel` (`id`, `mapel_id`, `dosen_id`, `status`) VALUES
(1, 1, 6, 0),
(2, 2, 1, 0),
(3, 3, 1, 0),
(4, 3, 8, 0),
(5, 2, 1, 0),
(6, 5, 9, 1),
(7, 6, 6, 0),
(8, 7, 6, 1),
(9, 8, 10, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_guru`
--
ALTER TABLE `data_guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_kelas`
--
ALTER TABLE `data_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_siswa`
--
ALTER TABLE `data_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_kelas`
--
ALTER TABLE `detail_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_mapel`
--
ALTER TABLE `detail_mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontenmateri`
--
ALTER TABLE `kontenmateri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_dislike`
--
ALTER TABLE `like_dislike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progress`
--
ALTER TABLE `progress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progress_belajar`
--
ALTER TABLE `progress_belajar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `submateri`
--
ALTER TABLE `submateri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `tugas`
--
ALTER TABLE `tugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jadwal`
--
ALTER TABLE `t_jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_mapel`
--
ALTER TABLE `t_mapel`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;

--
-- AUTO_INCREMENT for table `data_guru`
--
ALTER TABLE `data_guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `data_kelas`
--
ALTER TABLE `data_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `data_siswa`
--
ALTER TABLE `data_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `detail_kelas`
--
ALTER TABLE `detail_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `detail_mapel`
--
ALTER TABLE `detail_mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kontenmateri`
--
ALTER TABLE `kontenmateri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `like_dislike`
--
ALTER TABLE `like_dislike`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mata_pelajaran`
--
ALTER TABLE `mata_pelajaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `materi`
--
ALTER TABLE `materi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `progress`
--
ALTER TABLE `progress`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `progress_belajar`
--
ALTER TABLE `progress_belajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `submateri`
--
ALTER TABLE `submateri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tugas`
--
ALTER TABLE `tugas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_jadwal`
--
ALTER TABLE `t_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_mapel`
--
ALTER TABLE `t_mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
