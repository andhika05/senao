<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {
	var $username = '';

	public function __construct()
    {
        parent::__construct();

        // MODEL
        $this->load->model("Siswa_model");

        // JS
		$data['js'] = '';
		$data['validasi'] = '';

        // CHECK LOGIN
        if(!$this->session->userdata('level')){	
        	$this->session->set_flashdata("error","Anda harus login untuk mengakses halaman ini ");
        	redirect("../auth/login");
        }else if($this->session->userdata('level') < 2){
        	redirect("../guru");
        }

        $this->mapel 	= $this->session->userdata('mapel');
        $this->mapel_id = $this->session->userdata('id_mapelqu');
        $this->username = $this->session->userdata('username');
        $this->userid 	= $this->session->userdata('userid');
    }
	

	public function index()
	{
		if($this->session->userdata('mapel')){
			redirect("beranda/materi");
		}

		$data['js'] = '';
		$data['validasi'] = '';

		$data['mapel'] = $this->Siswa_model->get_mapel_siswa($this->username);
		$this->load->view('template/header');
		$this->load->view('beranda/mapel', $data);
		$this->load->view('template/footer');
	}

	public function materi()
	{
		if(!$this->session->userdata('mapel')){
			redirect("beranda");
		}

		$data['js'] = '';
		$data['validasi'] = '';

		//HASIL PROGRESS
		$data['hasilprogress'] = $this->Siswa_model->progress_percentage($this->mapel);
		$data['materi'] = $this->Siswa_model->get_materi_list($this->mapel_id);

		// print_r($this->mapel);
		// die();
		$this->load->view('template/header');
		$this->load->view('beranda/index', $data);
		$this->load->view('template/footer');
	}

	public function log_mapel()
	{

		$data['js'] = '';
		$data['validasi'] = '';

		//HASIL PROGRESS
		// $data['hasilprogress'] = $this->Siswa_model->progress_percentage($this->mapel);
		$data['mapel'] = $this->Siswa_model->get_transaksi();

		$this->load->view('template/header');
		$this->load->view('beranda/log_mapel', $data);
		$this->load->view('template/footer');
	}

	public function upload_bukti($id)
	{
		if($_POST){
			$folder_upload_bukti   =   '../upload/bukti/';

			// UPDATE FOTO PROFIL
			if (!empty($_FILES['bukti']['name'])) {
				// UPLOAD FOTO PROFIL
		        $config['upload_path']          = FCPATH.$folder_upload_bukti;
	            $config['allowed_types']        = 'gif|jpg|png|jpeg|pdf|docx|zip';
	            $config['max_width']			= 10240;
	            $config['max_height']			= 7680;

	            // RENAME
	            // $rep = str_replace(" ", "_", $nama_mapel);
	            $foto 							= $id."-".$_FILES["bukti"]['name'];
	            $config['file_name']			= $foto;

				$this->load->library('upload');
				
				$this->upload->initialize($config);

				if($this->upload->do_upload('bukti')) {
	                // CHANGE MODE\
	                chmod($config['upload_path'].$foto, 0777); // note that it's usually changed to 0755
	                $this->Siswa_model->upload_bukti($id, $foto);
	            }
	            else{
					$this->session->set_flashdata('message','<label class="label label-danger clues">Jenis file untuk foto profil tidak didukung</label>');
	            }
			}

		}
		redirect('beranda/log_mapel');
	}

	public function mapel($id,$id_mapelqu){
		$this->session->set_userdata('mapel', $id);
		$this->session->set_userdata('id_mapelqu', $id_mapelqu);
		redirect("beranda/materi");
	}

	public function unset_mapel(){
		$this->session->unset_userdata('mapel');
		redirect("beranda/materi");
	}	
}
