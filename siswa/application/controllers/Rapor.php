<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rapor extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        // MODEL
        $this->load->model("Siswa_model");

        // CHECK LOGIN
        if(!$this->session->userdata('level')){
        	$this->session->set_flashdata("error","Anda harus login untuk mengakses halaman ini ");
        	redirect("../auth/login");
        }
        // CHECK MAPEL
        if(!$this->session->userdata('mapel')){
			redirect("beranda");
		}

        $this->mapel 	= $this->session->userdata('mapel');
        $this->mapel_id = $this->session->userdata('id_mapelqu');
        $this->username = $this->session->userdata('username');
        $this->userid   = $this->session->userdata('userid');
    }

	public function index(){
		if(!$this->session->userdata('id_mapelqu')){
			redirect("beranda");
		}
		$data['js'] = '';
		$data['validasi'] = '';
		$data['modal'] = '';

		//DAFTAR MATERI DAN NILAI
		$data['hasilprogress'] = $this->Siswa_model->progress_percentage($this->mapel);
		$data['materi'] = $this->Siswa_model->get_materi_list($this->mapel_id);
		$data['rapor'] = array();

		$this->load->view('template/header');
		$this->load->view('rapor/index', $data);
		$this->load->view('template/footer');
	}

	public function print(){
		$data['siswa'] = $this->Siswa_model->get_siswa_detail($this->session->userdata('userid'));
		$data['materi'] = $this->Siswa_model->get_materi_rapor($this->mapel_id);
		$this->load->view('rapor/print', $data);
	}

	public function printsertifikat($id){
		$this->mapel 	= $this->session->userdata('mapel');
        $this->username = $this->session->userdata('username');
        $this->userid = $this->session->userdata('userid');
		$a['data']	= $this->Siswa_model->get_mapel_siswa($id);	
		$this->load->view('rapor/printSertifikat', $a);
	}
}
