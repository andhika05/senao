<div class="container">
    <div class="row">
        <h1 class="reg-heading" style="margin-left:37%;">Profile</h1>
    </div>
</div>

<section class="profil-siswa" style="margin-top: -0.5%;">
    <div class="container">
        <div class="row">
            <?php
                    if(is_array($profil)){
                        foreach($profil as $data){
            ?>
            <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                <div class="pp-profil img-circle">
                    <img src="<?php echo base_url('../upload/foto/siswa/').$data['foto']; ?>" alt="Foto Profil Siswa" class="img-responsive">
                </div>
            </div>
            <div class="col-lg-8 col-xs-12" style="margin-left: 10%; margin-top: 15px;">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <label class="profil-head">Username</label>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                        <span>:</span>
                        <label><?php echo $data['username']; ?></label>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <label class="profil-head">Nama Lengkap</label>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                        <span>:</span>
                        <label><?php echo $data['nama_siswa']; ?></label>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <label class="profil-head">Email</label>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                        <span>:</span>
                        <label><?php echo $data['email']; ?></label>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <label class="profil-head">No. KTP</label>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                        <span>:</span>
                        <label><?php echo $data['nim']; ?></label>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                        <label class="profil-head">Testimoni</label>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                        <span>:</span>
                        <label><?php echo $data['testimoni']; ?></label>
                    </div>
                    
                </div>

                <div class="row" style="margin-left:10%;">
                    <div class="form-reg">
                        <a href="<?php echo base_url('profil/ubah')?>" class="btn btn-warning-custom">Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
                        }
                } else {
        ?>
                    <div class="container">
                        <div class="row materi-msg">
                            <div class="item-reg text-center">
                                    <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
                            </div>
                        </div>
                    </div>
        <?php        
                }
        ?>
    </div>
</section>