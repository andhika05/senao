<div class="container">
    <div class="row">
        <h1 class="reg-heading">Form Ubah Profil Siswa</h1>
    </div>
</div>


<?php
        if(is_array($profil)){
            foreach($profil as $data){
?>
<section class="form-reg">
    <div class="container">
        <form class="form-group" role="form" name="formedprofil" id="formedprofil" action="<?php echo base_url('profil/doubah')?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="username" value="<?php echo $this->session->userdata('username'); ?>">
            <input type="hidden" name="id" value="<?php echo $data['id_siswa']; ?>">


            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="nama" class="control-label">Nama Lengkap</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="text" name="nama" class="form-control" id="nama" value="<?php echo $data['nama_siswa']; ?>"></div>
            </div>

            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="absen" class="control-label">No. KTP</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="number" name="absen" class="form-control" id="absen" value="<?php echo $data['nim']; ?>">
                </div>
            </div>

            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="mail" class="control-label">Email</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <input type="email" name="mail" class="form-control" id="mail" value="<?php echo $data['email']; ?>">
                </div>
            </div>

            <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="profil" class="control-label">Foto Profil</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12 foto-profil">
                    <div class="pp-edit img-circle">
                        <img src="<?php echo base_url('../upload/foto/siswa/').$data['foto']; ?>" alt="Foto Profil Siswa" class="img-responsive" name="foto">
                    </div>
                    <input name="profil" type="file" class="custom-file-input">
                </div>
            </div>
             <div class="row item-reg">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <label for="nama" class="control-label">Testimoni</label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
                    <textarea name="testimoni" class="form-control" id="testimoni"><?php echo $data['testimoni'] ?></textarea></div>
            </div>
            <div class="col-md-offset-3">
                <input type="submit" name="finish_reg" value="Selesai" class="btn btn-default">
            </div>
        </form>
    </div>
</section>
<?php
            }
    } else {
?>
<div class="container">
    <div class="row materi-msg">
        <div class="item-reg text-center">
                <label class="label label-danger" style="color:white;">Data tidak ditemukan</label>
        </div>
    </div>
</div>
<?php
    }
?>
