<html>
    <head>
        <?php $halaman = ucfirst($this->router->fetch_class()); ?>
        <title> <?php echo $halaman ?> Mahasiswa </title>
        
        <!-- Meta Tags -->
        <meta charset="UTF-8">
        <meta name="description" content="E-Learning">
        <meta name="keywords" content="Courses, Class, About,">
        <meta name="author" content="Tim E-Learning">
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- CSS -->

        <base href="http://<?php echo $_SERVER['HTTP_HOST'];?>/e-learning/siswa">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>bootstrap.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>style.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>siswa.css"/>

        <!-- Icons -->
<!--         <link rel="icon" href="<?php echo base_url('assets/images/')?>logo1.png">
 -->        
        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

                <!-- Meta Tags -->
        <meta charset="UTF-8">
        <meta name="description" content="E-Learning">
        <meta name="keywords" content="E-Learning">
        <meta name="author" content="E-Learning">
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        
        <!-- CSS -->

        <base href="http://<?php echo $_SERVER['HTTP_HOST'];?>/e-learning/siswa">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>bootstrap.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>style.css"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/')?>siswa.css"/>

        <!-- Icons -->
        <!-- <link rel="icon" href="<?php echo base_url('assets/images/')?>logo1.png"> -->
        
        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        
        
    </head>
    <body>
        <header>
            <div class="navigation">
                <div class="container">
                    <div class="row">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand visible-xs" href="#">E-Learning</a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                              
                                    <?php
                                        $halaman        =   strtolower($halaman);
                                        if($halaman == '')  $halaman    =   'profil';

                                        $classbn        = "";
                                        $spanclassbn    = "";
                                        $classprof      = "";
                                        $spanclasspr    = "";
                                        $classrapor     = "";
                                        $spanclassrp    = "";
                                        $classmateri    = "";
                                        $spanclassmt    = "";
                                        $classnote      = "";
                                        $spannote       = "";

        
                                    ?>
                                    <ul class="nav navbar-nav">
                                        <li class="<?php echo $halaman == 'profil' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('profil');?>">Profile</a>
                                            <span class="<?php echo $halaman == 'profil' ? 'profile' : '' ?>"></span>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('beranda/unset_mapel')?>">My Courses</a>
                                        </li>
                                        <li class="<?php echo $halaman == 'materi' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('materi');?>">Materi</a>
                                            <span class="<?php echo $halaman == 'materi' ? 'materi' : '' ?>"></span>
                                        </li>
                                        <li class="<?php echo $halaman == 'rapor' ? 'active' : '' ?>">
                                            <a href="<?php echo base_url('rapor'); ?>">Record</a>
                                            <span class="<?php echo $halaman == 'rapor' ? 'clue' : '' ?>"></span>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('beranda/log_mapel')?>">Transactions</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url('../auth/keluar')?>">Keluar</a>
                                        </li>
                                    </ul>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><label class="label label-warning"><?php echo $this->session->userdata("username"); ?></label></a>
                                            <div class="dropdown-menu" aria-labelledby="dropdown05" style="text-align: center;">   
                                              <a href="<?php echo base_url('../home/courses')?>">Home</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
        </header>