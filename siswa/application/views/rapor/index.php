<section class="form-reg">
    <div class="container">
        <div class="row reg-heading">
            <h1 class="text-center">Rapor</h1>
            <br>
        </div>
    </div>
    <div class="container">
        <div class="row rapor">
            <div class="table-responsive">
                <table border="1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Materi</th>
                            <th style="text-align: center;">Submateri</th>
                            <th style="text-align: center;">Keterangan</th>
                        </tr>
                    </thead>

<?php
    if(is_array($materi)){
?>
                    <tbody>


<?php
	// FETCH MATERI
	$no_materi 	=	1;
	foreach($materi as $data_materi){
		$no_sub 	=	1;
		// GET TOTAL SUBMATERI IN THIS MATERI
		$total_sub	=	getSubMateriTotal($data_materi['id_materi']);
		// GET SUBMATERI OF THIS MATERI
		$submateri 	=	getSubMateri($data_materi['id_materi']);
		// FETCH

		foreach($submateri as $submateri){
			$nilaiclass	=	getNilaiClass($submateri['id']);
?>
                        <tr>
<?php 	echo ($no_sub == 1) ? '<td style="text-align:center;" rowspan="'.$total_sub.'"><b>'.$data_materi['nama_materi'].'</b></td>' : '' ?>
                            <td style="text-align: center;"><?php echo $submateri['nama']?></td>
                                <td style="text-align:center">
                                    <?php
                                        // SHOW STATUS
                                        if($nilaiclass > 55){
                                            echo "<label style='color:green'>Lulus</label>";
                                        }
                                        else if($nilaiclass > 0){
                                            echo "<label style='color:darkred'>Tidak Lulus</label>";
                                        }
                                        else{
                                            echo "<label style='color:orange'>Belum Ada Nilai</label>";
                                        }
                                    ?>
                                </td>
                        </tr>
<?php
			$no_sub++;
		}// END FETCH SUBMATERI
		$no_materi++;
	}// END FETCH MATERI
?>
                    </tbody>
<?php
} // ENDIF
?>
                </table>
            </div>
            <?php if($hasilprogress<100){ ?>
                <button type="button" class="btn btn-default" style="background: grey; border-bottom: 5px solid grey">Sertifikat</button>
            <?php } else { ?>
                <a href="<?php echo base_url() ?>Rapor/print" target="_blank"><button type="button" class="btn btn-default">Sertifikat</button></a>
            <?php } ?>

        </div>
    </div>
</div>
</section>