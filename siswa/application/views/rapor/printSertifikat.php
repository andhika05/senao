<!DOCTYPE html>
<html>
<head>
    <style type="text/css" media="print">
        table {
            border-collapse: collapse; 
            width: 100%;
        }
        td {
            padding: 7px 5px;
            text-align: left;
        }
        .container{
            box-sizing: inherit;
        }
        .logodisp {
            float: left;
            position: relative;
            width: 80px;
            height: 80px;
            margin: .5rem 0 0 .5rem;
        }
        .up {
            text-transform: uppercase;
            margin: 0;
            line-height: 2.2rem;
            font-size: 1.5rem;
            font-size: 17px!important;
            font-weight: normal;
        }
        .disp {
            text-align: left;
            margin: -.5rem 0;
        }
        #nama {
            font-size: 20px!important;
            font-weight: bold;
            text-transform: uppercase;
            margin: -10px 0 -2px 0;
        }
        #alamat {
            margin-top: -15px;
            font-size: 13px;
        }
        #lokasi {
            margin-top: -2px;
            font-size: 13px;
        }
        .separator {
            border-bottom: 2px solid #616161;
            margin: 1rem 0 1rem;
            width: 100%;
        }
    </style>

    <style type="text/css" media="screen">
        table {
            border-collapse: collapse; 
            width: 60%; 
        }
        td {
            padding: 7px 5px;
            text-align: left;
        }
        .container{
            box-sizing: inherit;
        }
        .logodisp {
            float: left;
            position: relative;
            width: 80px;
            height: 80px;
            margin: .5rem 0 0 .5rem;
        }
        .up {
            text-transform: uppercase;
            margin: 0;
            line-height: 2.2rem;
            font-size: 17px!important;
            font-weight: normal;
        }
        .disp {
            text-align: center;
            margin: -.5rem 0;
        }
        #nama {
            font-size: 20px!important;
            font-weight: bold;
            text-transform: uppercase;
            margin: -10px 0 -2px 0;
        }
        #alamat {
            margin-top: -4px;
            font-size: 13px;
        }        
        #lokasi {
            margin-top: 0;
            font-size: 13px;
        }
        .separator {
            border-bottom: 2px solid #616161;
            margin: 1rem 0 2rem;
            width: 60%;
        }
    </style>
</head>
<body onload="window.print()">
    <div class="container">
        <div class="disp">
            <table align="center">
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <img class="logodisp" src="<?php echo base_url() ?>upload/sertifikat/mbuntu-1.jpg">
                        <h6 class="up">Gloed</h6>
                        <h5 class="up" id="nama">Elearning</h5>
                        <h4 id="lokasi">Jalan. Soekarno Hatta</h4>
                    </td>
                </tr>
            </table>
        </div>
        <center><div class="separator"></div></center>
        <div class="disp">
            <?php foreach($data as $d) { ?>
           <img src="<?php echo base_url()?>../upload/sertifikat/mbuntu-1.jpg">
           <?php echo $d['mata_pelajaran'] ?></td>
            <?php } ?>
        </div>
    </div>
</body>
</html>