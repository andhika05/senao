<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<title>Krogarna.se Certificate</title>

<style type="text/css" media="all">
    #top {
height: 100%;
}
#position_me {
left: 0;
}

    .SlideBackGround
    {
        height:100%;
        width:100%;
        
        background-color:white;
        background-image:url(<?php echo base_url() ?>assets/images/sertifikat/front.png);
        background-size: 100% 100%;
        background-repeat:no-repeat;
        z-index: 2;

    }
    .SlideBackGround2
    {
        height:100%;
        width:100%;

        background-color:white;
        background-image:url(<?php echo base_url() ?>assets/images/sertifikat/back1.png);
        background-size: 100% 100%;
        background-repeat:no-repeat;
        z-index: 2;

    }
    
    .MiddleLine
    {
    width:720px;
    position: absolute;
    left:170px;
    top:380px;
    z-index:11;
    color:black;
    }
    
    .StudentName
    {
    font-family: "Calibri";
    font-weight:bold;
    height:200px;
    width:720px;
    position:absolute;
    left:170px;
    top:340px;
    font-size:32px;
    text-align:center;
    z-index:11;
    color:black;
    }

    .InstrukturName
    {
    font-family: "Calibri";
    font-weight:bold;
    height:200px;
    width:520px;
    position:absolute;
    /*left:70px;*/
    top:670px;
    font-size:24px;
    text-align:center;
    z-index:11;
    color:black;
    }

    .DirekturName
    {
    font-family: "Calibri";
    font-weight:bold;
    height:200px;
    width:520px;
    position:absolute;
    /*left:70px;*/
    right: 0px;
    top:670px;
    font-size:24px;
    text-align:center;
    z-index:11;
    color:black;
    }

    .Conten
    {
    font-family: "Calibri";
    font-weight:bold;
    height:600px;
    width:850px;
    position:absolute;
    /*left:70px;*/
    left: 90px;
    top:950px;
    font-size:24px;
    /*text-align:center;*/
    z-index:11;
    color:black;
    /*border: 1px solid black;*/
    }

    html, body {
            height: 100%;
            margin: 0;
        }

    
    @media print {
        * {
    -webkit-print-color-adjust: exact !important;   /* Chrome, Safari */
    color-adjust: exact !important;                 /*Firefox*/
    margin: 0;
    }

    @page { margin: 0; size: landscape;}
}
</style>
</head>

<body onload="window.print()">
<div class="SlideBackGround">
</div>

<hr class="MiddleLine" />

<div class="StudentName"><?php echo $siswa[0]['nama'] ?></div>

<div class="SlideBackGround2">
</div>

<div class="InstrukturName"><?php echo $materi[0]['nama_dosen'] ?></div>
<div class="DirekturName"><?php echo "Leonardo Kamajaya" ?></div>

<div class="Conten">
    <?php $num=1; foreach($materi as $m) { ?>
        <p><?php echo $num; ?>. <?php echo $m['nama_materi']; ?></p>
    <?php $num++; } ?>
</div>
</body>
</html>