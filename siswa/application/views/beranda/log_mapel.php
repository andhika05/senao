<section class="form-reg">
    <div class="container">
        <div class="row reg-heading">
            <h1 class="text-center">Log Transaksi</h1>
            <br>
        </div>
    </div>
    <div class="container">
        <div class="row rapor">
            <div class="table-responsive">
                <table border="1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Nomor</th>
                            <th>Mata Pelajaran</th>
                            <th>Tanggal Transaksi</th>
                            <th>Bukti Transfer</th>
                            <th>Status</th>
                        </tr>
                    </thead>

<?php
    if(is_array($mapel)){
?>
                    <tbody>
<?php
	// FETCH MATERI
	$no_mapel 	=	1;
	foreach($mapel as $data_mapel){
		
?>
                        <tr>
                            <td><?php echo $no_mapel ?></td>
                            <td><?php echo $data_mapel['nama_mapel']?></td>
                            <td><?php echo $data_mapel['tgl_transaksi']?></td>
                            <td>
                                <?php if($data_mapel['upload_bukti'] == null){ ?>
                                    <form action="<?php echo base_url() ?>beranda/upload_bukti/<?php echo $data_mapel['id_transaksi'] ?>" method="post" enctype="multipart/form-data">
                                        <input type="file" name="bukti"><input type="submit" name="upload" value="Upload">
                                    </form>
                                <?php } else {
                                    echo "Telah Upload Bukti";
                                    } ?>
                            </td>
                            <td>
                                <?php if($data_mapel['stat']=="Sudah Verifikasi"){ ?>
                                    <label class="label label-success"><?php echo $data_mapel['stat']; ?></label>
                                <?php } else { ?>
                                    <label class="label label-danger"><?php echo $data_mapel['stat']; ?></label>
                                <?php } ?>
                            </td>
                        </tr>
<?php
		$no_mapel++;
	}// END FETCH MATERI
?>
                    </tbody>
<?php
} // ENDIF
?>
                </table>
            </div>
        </div>
    </div>
</div>
</section>