    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/elegant-fonts.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/swiper.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/')?>css/stylecourses.css">

  <body>

    <section class="featured-courses vertical-column courses-wrap">
        <div class="container">
            <div class="row mx-m-25">
                <div class="col-12 px-25">
                    <div class="mb-5 element-animate text-center" style="margin-top: -5%;">
                      <h1>Mata pelajaran</h1>
                       <p class="lead">Mata pelajaran yang anda ikuti</p>
                    </div>
                </div>
                
                <?php foreach ($mapel as $mapel) { ?>                
                    <div class="col-12 col-md-6 col-lg-4 px-25">
                        <div class="course-content">
                            <figure class="course-thumbnail">
                                <a href="<?php echo base_url().'beranda/mapel/'.$mapel['id_mata'].'/'.$mapel['id_mapelqu']?>" class="course">
                                <img src="<?php echo base_url('../upload/foto/mapel/'.$mapel['foto']) ?>" class="img-responsive " alt="" style="height: 180px;">
                                <h4 style="text-align: center;"><?php echo $mapel['nama_mapel']?></h4></a>
                            </a>
                            </figure>
                        </div>
                    </div>
                   <?php } ?>
             </div>
        </div>    
    </section>
    

    <!-- Anyar -->
    <script src='<?php echo base_url('assets/')?>js/jquery.js'></script>
    <script src='<?php echo base_url('assets/')?>js/swiper.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/masonry.pkgd.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/jquery.collapsible.min.js'></script>
    <script src='<?php echo base_url('assets/')?>js/custom.js'></script>

    <script src="<?php echo base_url('assets/')?>js/main.js"></script>
  </body>
